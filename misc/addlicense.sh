#!/bin/sh

REPO_ROOT_DIR="$(git rev-parse --show-toplevel)"
LICENSE_TEXT_FILE="${REPO_ROOT_DIR}/aux/license.header"
LICENSELEN=$(wc -l "${LICENSE_TEXT_FILE}" | cut -f 1 -d ' ')
for x in $*; do
	head -$LICENSELEN $x | diff "${LICENSE_TEXT_FILE}" - || ( ( cat "${LICENSE_TEXT_FILE}"; echo; cat $x) > /tmp/file; mv /tmp/file $x )
done
