# Find module for the gumbo parser library (https://github.com/google/gumbo-parser)
#
# set GUMBO_USE_STATIC_LIBS to ``TRUE`` to look for static libraries.
# Gumbo_FOUND if the library was found
# Gumbo::Gumbo target to link to

include(CheckIncludeFile)

if (NOT Gumbo_FOUND)
	include(FindPackageHandleStandardArgs)

	# try pkg-config first
	find_package(PkgConfig)
	if (PKG_CONFIG_FOUND)
		if (${CMAKE_FIND_PACKAGE_NAME}_FIND_VERSION)
			set(__module "gumbo>=${${CMAKE_FIND_PACKAGE_NAME}_FIND_VERSION}")
		else()
			set(__module "gumbo")
		endif()
		pkg_check_modules(GumboViaPkgConfig IMPORTED_TARGET GLOBAL "${__module}")
		if (GumboViaPkgConfig_FOUND)
			find_package_handle_standard_args(Gumbo
				REQUIRED_VARS GumboViaPkgConfig_LIBRARIES
				VERSION_VAR GumboViaPkgConfig_VERSION
			)
			add_library(Gumbo::Gumbo ALIAS PkgConfig::GumboViaPkgConfig)
		endif()
	endif()

	if (NOT Gumbo_FOUND)
		# manual mode
		find_path(__gumbo_INCLUDE_DIR gumbo.h PATH_SUFFIXES "gumbo")
		if (GUMBO_USE_STATIC_LIBS)
			set(__gumbo_static_filename "${CMAKE_STATIC_LIBRARY_PREFIX}gumbo${CMAKE_STATIC_LIBRARY_SUFFIX}")
		endif()
		find_library(__gumbo_library NAMES ${__gumbo_static_filename} gumbo)
		string(FIND "${__gumbo_library}" "${CMAKE_STATIC_LIBRARY_SUFFIX}" __staticSuffixIdx REVERSE)
		if(__staticSuffixIdx EQUAL -1)
			set(__libType SHARED)
		else()
			set(__libType STATIC)
		endif()
		find_package_handle_standard_args(Gumbo
				REQUIRED_VARS __gumbo_INCLUDE_DIR __gumbo_library)
		if (Gumbo_FOUND)
			add_library(Gumbo::Gumbo ${__libType} IMPORTED)
			set_target_properties(Gumbo::Gumbo PROPERTIES
				IMPORTED_LOCATION ${__gumbo_library}
				IMPORTED_LINK_INTERFACE_LANGUAGES C)
			target_include_directories(Gumbo::Gumbo SYSTEM INTERFACE ${__gumbo_INCLUDE_DIR})
		endif()
	endif()
endif()
