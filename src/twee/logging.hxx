/*
    Copyright © 2020 ezsh. All rights reserved.
    Use of this source code is governed by a Simplified BSD License which
    can be found in the LICENSE file.
*/

#ifndef TWEE_LOGGING_HXX
#define TWEE_LOGGING_HXX

#include <spdlog/spdlog.h>

#ifdef SPDLOG_FMT_EXTERNAL
#	include <fmt/ostream.h>
#else
#	include <spdlog/fmt/ostr.h>
#endif

#include <filesystem>
#include <memory>
#include <vector>

#include "twee-export.h"

namespace twee::logging {
	TWEE_API std::shared_ptr<spdlog::logger> setupLogger(const std::vector<spdlog::sink_ptr>& sinks);
	TWEE_API std::shared_ptr<spdlog::logger> setupLogger(const std::filesystem::path& logFile, bool consoleLog = true);
} // namespace twee::logging

#endif
