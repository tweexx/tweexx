/*
    Copyright © 2020 ezsh. All rights reserved.
    Use of this source code is governed by a Simplified BSD License which
    can be found in the LICENSE file.
*/

#include "./decompiler.hxx"

#include "./escaping.hxx"
#include "./io.hxx"
#include "./logging.hxx"
#include "./story.hxx"
#include "./types.hxx"

#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/constants.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/trim.hpp>

#include <gumbo.h>

#include <charconv>
#include <memory>
#include <regex>
#include <stdexcept>
#include <string_view>
#include <system_error>
#include <vector>

namespace {
	using string_view = twee::string_view;
	using GumboIndex = decltype(GumboVector::length);

	void cleanGumbo(GumboOutput* output)
	{
		gumbo_destroy_output(&kGumboDefaultOptions, output);
	}
#if 0
	bool startsWith(const GumboStringPiece& gs, const std::string_view& s)
	{
		if (gs.length < s.size()) { return false; }
		for (std::size_t i = 0; i < s.size(); ++i) {
			if (gs.data[i] != s[i]) { return false; }
		}
		return true;
	}
#endif
	std::string_view substr(GumboStringPiece gs, std::size_t start = 0, std::size_t length = string_view::npos)
	{
		if (length == string_view::npos) {
			length = gs.length - start;
		} else if (gs.length < start + length) {
			throw std::out_of_range("requested substring is longer than the source string");
		}

		return {gs.data + start, length};
	}

	bool tagIs(const GumboElement& e, const string_view& tag) noexcept
	{
		if (e.original_tag.length < tag.size() + 2) {
			return false;
		}
		return (e.original_tag.data[0] == '<') &&
			(e.original_tag.data[tag.length() + 1] == '>' || e.original_tag.data[tag.length() + 1] == ' ') &&
			(substr(e.original_tag, 1, tag.length()) == tag);
	}

	const GumboAttribute* findAttribute(const GumboElement& e, string_view attributeName) noexcept
	{
		for (GumboIndex i = 0; i < e.attributes.length; ++i) {
			if (static_cast<const GumboAttribute*>(e.attributes.data[i])->name == attributeName) {
				return static_cast<const GumboAttribute*>(e.attributes.data[i]);
			}
		}
		return nullptr;
	}

	const GumboNode* findChildTag(const GumboNode* node, string_view tag)
	{
		if (node->type == GUMBO_NODE_ELEMENT) {
			const GumboVector children = node->v.element.children;
			for (GumboIndex i = 0; i < children.length; ++i) {
				const GumboNode* child = static_cast<const GumboNode*>(children.data[i]);
				if (child->type != GUMBO_NODE_ELEMENT) { continue; }
				if (tagIs(child->v.element, tag))
				{
					assert(child->v.element.tag == GUMBO_TAG_UNKNOWN); // otherwise use the other overload
					return child;
				}
			}
		}
		return nullptr;
	}

	const GumboNode* findChildTag(const GumboNode* node, GumboTag tag) noexcept
	{
		if (node->type == GUMBO_NODE_ELEMENT) {
			const GumboVector& children = node->v.element.children;
			for (GumboIndex i = 0; i < children.length; ++i) {
				const GumboNode* child = static_cast<const GumboNode*>(children.data[i]);
				if (child->type != GUMBO_NODE_ELEMENT) { continue; }
				if (child->v.element.tag == tag) { return child; }
			}
		}
		return nullptr;
	}

	template<class F>
	void forEachAttribute(const GumboElement& elem, F&& func)
	{
		const auto& attributes = elem.attributes;
		for (GumboIndex i = 0; i < attributes.length; ++i) {
			const GumboAttribute* a = static_cast<GumboAttribute*>(attributes.data[i]);
			auto make_string_view = [](const char* begin, unsigned size) -> std::string_view {
				return std::string_view(begin, ::strnlen(begin, size));
			};

			func(make_string_view(a->name, a->name_end.offset - a->name_start.offset),
				 make_string_view(a->value, a->value_end.offset - a->value_start.offset));
		}
	}

#if 0
	const GumboNode* findChildTag(const GumboNode* node, const std::vector<std::string>& path)
	{
		for (const auto& tag: path) {
			node = findChildTag(node, tag);
			if (!node) {
				return nullptr;
			}
		}
		return node;
	}
#endif

	const GumboNode* findElementById(const GumboNode* node, const std::regex& id)
	{
		if (node->type == GUMBO_NODE_ELEMENT) {
			GumboVector children = node->v.element.children;
			for (GumboIndex i = 0; i < children.length; ++i) {
				const GumboNode* child = static_cast<const GumboNode*>(children.data[i]);
				if (child->type != GUMBO_NODE_ELEMENT) { continue; }
				const auto* idAttr = findAttribute(child->v.element, "id");
				if (idAttr && std::regex_match(idAttr->value, id)) { return child; }
			}
		}
		return nullptr;
	}

	std::string elementContent(const GumboElement& elem)
	{
		if (!elem.children.length) { return {}; }
		const auto* firstChild = static_cast<const GumboNode*>(elem.children.data[0]);
		if (firstChild->type == GUMBO_NODE_WHITESPACE) { return {}; }
		if (firstChild->type == GUMBO_NODE_TEXT) {
			std::string res = firstChild->v.text.text;
			boost::algorithm::trim(res);
			return res;
		}
		throw std::logic_error("Execution shall not reach this point");
	}

	std::vector<std::string> getFields(const std::string_view fieldsStr)
	{
		if (!fieldsStr.empty()) {
			std::vector<std::string> res;
			boost::algorithm::split(res, fieldsStr, boost::is_space(), boost::algorithm::token_compress_on);
			return res;
		}
		return {};
	}

	constexpr const string_view tagTwStoryData = "tw-storydata";
	constexpr const string_view tagTwTag = "tw-tag";
	constexpr const string_view tagTwPassageData = "tw-passagedata";

	constexpr const string_view attributeName = "name";
	constexpr const string_view attributeStartNode = "startnode";
	constexpr const string_view attributeCreator = "creator";
	constexpr const string_view attributeCreatorVersion = "creator-version";
	constexpr const string_view attributeIFID = "ifid";
	constexpr const string_view attributeZoom = "zoom";
	constexpr const string_view attributeFormat = "format";
	constexpr const string_view attributeFormatVersion = "format-version";
	constexpr const string_view attributeOptions = "options";
	constexpr const string_view attributePassageDataPid = "pid";
	constexpr const string_view attributePassageDataName = "name";
	constexpr const string_view attributePassageDataTags = "tags";

	constexpr const string_view attributeTiddler = "tiddler";
	constexpr const string_view attributeTwinePosition = "twine-position";
	constexpr const string_view attributeTwineCreated = "created";
	constexpr const string_view attributeTwineModified = "modified";

	constexpr const string_view attributeColorName = "name";
	constexpr const string_view attributeColorValue = "color";
} // namespace

twee::Story twee::decompileHTML(const std::filesystem::path& filename, string_view encoding)
{
	Story s{filename.stem().string()}; // the name is fallback
	spdlog::trace("Loading HTML file {}", fmt::streamed(filename));
	const std::string contents = io::readAllWithEncoding(filename, encoding);
	const GumboOptions options = kGumboDefaultOptions;
	spdlog::trace("Running HTML parser");
	std::unique_ptr<GumboOutput, decltype(&cleanGumbo)> gumbo_output{
	    gumbo_parse_with_options(&options, contents.data(), contents.length()), cleanGumbo};

	spdlog::trace("HTML parsing done");
	GumboNode* doc = gumbo_output->root;

	// Twine 1 inserts build info as a comment within <head>, try to locate it and print out
	if (const GumboNode* head = findChildTag(doc, GUMBO_TAG_HEAD)) {
		const GumboVector& children = head->v.element.children;
		for (GumboIndex i = 0; i < children.length; ++i) {
			const GumboNode* child = static_cast<const GumboNode*>(children.data[i]);
			if (child->type != GUMBO_NODE_COMMENT) { continue; }
			const GumboText& text = child->v.text;
			if (const char* buildInfo = std::strstr(text.text, "Build Info:")) {
				std::string bi{buildInfo};
				std::replace_if(bi.begin(), bi.end(), [](char c){return c == '\n';}, ' ');
				spdlog::info(bi);
				break;
			}
		}
	}

	const GumboNode* body = findChildTag(doc, GUMBO_TAG_BODY);
	if (!body) {
		spdlog::critical("Could not find the <body> tag in the HTML file {}", fmt::streamed(filename));
		throw std::runtime_error("Invalid story HTML");
	}

	const GumboNode* storyStoreNode = nullptr;
	const GumboNode* storyDataNode = findChildTag(body, tagTwStoryData);
	if (!storyDataNode) {
		// don't give up yet, it might be inside story-area
		storyStoreNode = findElementById(body, std::regex("store(?:-a|A)rea"));
		if (!storyStoreNode) {
			spdlog::critical("Could not find story content in the HTML file {}", fmt::streamed(filename));
			throw std::runtime_error("Invalid story HTML");
		}
		storyDataNode = findChildTag(storyStoreNode, tagTwStoryData);
	}

	if (storyDataNode) { // Twine 2 style story data chunk.
		/*
		    <tw-storydata name="…" startnode="…" creator="…" creator-version="…" ifid="…"
		        zoom="…" format="…" format-version="…" options="…" hidden>…</tw-storydata>
		*/
		long startNode = 0;
		std::string creator;
		std::string creatorVersion = "unknown";
		forEachAttribute(storyDataNode->v.element, [&](std::string_view attrName, std::string_view attrValue) {
			if (attrName == attributeName) {
				s.setName(std::string(attrValue));
			} else if (attrName == attributeStartNode) {
				std::from_chars_result r = std::from_chars(attrValue.begin(), attrValue.end(), startNode);
				if (r.ec != std::errc{}) {
					spdlog::error("Could not parse start passage id (integer number) from attribute string '{}': {}",
					    attrValue, std::make_error_code(r.ec).message());
				}
			} else if (attrName == attributeIFID) {
				s.metadata().ifid = attrValue;
			} else if (attrName == attributeZoom) {
				std::from_chars_result r =
					std::from_chars(attrValue.begin(), attrValue.end(), s.metadata().twine2.zoom);
				if (r.ec != std::errc{}) {
					spdlog::error(
						"Could not parse zoom value (floating point number) from attribute string '{}': {}", attrValue,
						std::make_error_code(r.ec).message());
				}
			} else if (attrName == attributeFormat) {
				s.metadata().twine2.format = attrValue;
			} else if (attrName == attributeFormatVersion) {
				s.metadata().twine2.formatVersion = attrValue;
			} else if (attrName == attributeOptions) {
				// FIXME: I'm unsure whether the `options` content attribute is
				// intended to be a space delimited list.  That does seem likely,
				// so we treat it as such for now.
				for (const auto& opt: getFields(attrValue)) { s.metadata().twine2.options.insert(opt); }
			} else if (attrName == attributeCreator) {
				creator = attrValue;
			} else if (attrName == attributeCreatorVersion) {
				creatorVersion = attrValue;
			}
		});

		if (!creator.empty()) {
			spdlog::info("The story file was created with '{}', version {}", creator, creatorVersion);
		}

		// story data nodes
		const auto& storyDataChildren = storyDataNode->v.element.children;
		for (GumboIndex i = 0; i < storyDataChildren.length; ++i) {
			const GumboNode* child = static_cast<const GumboNode*>(storyDataChildren.data[i]);
			if (child->type != GUMBO_NODE_ELEMENT) { continue; }

			const GumboElement& elem = child->v.element;

			long pid = 0;
			string name;
			std::filesystem::path path;
			std::vector<string> tags;
			string content;
			Passage::Metadata metadata;

			if (elem.tag == GUMBO_TAG_SCRIPT || elem.tag == GUMBO_TAG_STYLE) {
				/*
				    <style role="stylesheet" id="twine-user-stylesheet" type="text/twine-css">…</style>
				    <script role="script" id="twine-user-script" type="text/twine-javascript">…</script>
				*/
				// skip empty elements
				if (!elem.children.length) { continue; }
				content = elementContent(elem);
				if (content.empty()) {
					// NOTE: Skip elements that are empty after trimming; this additional
					// "empty" check is necessary because most (all?) versions of Twine 2
					// habitually append newlines to the nodes, so they're almost never
					// actually empty.
					continue;
				}
				if (elem.tag == GUMBO_TAG_SCRIPT) {
					name = "Story JavaScript";
					tags = {"script"};
				} else if (elem.tag == GUMBO_TAG_STYLE) {
					name = "Story Stylesheet";
					tags = {"stylesheet"};
				}
			} else if (tagIs(elem, tagTwTag)) {
				/*
				    <tw-tag name="…" color="…"></tw-tag>
				*/
				std::string tagName, tagColor;
				forEachAttribute(elem, [&](std::string_view attrName, std::string_view attrValue) {
					if (attrName == attributeColorName) {
						tagName = attrValue;
					} else if (attrName == attributeColorValue) {
						tagColor = attrValue;
					}
				});
				s.metadata().twine2.tagColors[tagName] = tagColor;
				continue;
			} else if (tagIs(elem, tagTwPassageData)) {
				/*
				    <tw-passagedata pid="…" name="…" tags="…" position="…" size="…">…</tw-passagedata>
				*/
				forEachAttribute(elem, [&](std::string_view attrName, std::string_view attrValue) {
					if (attrName == attributePassageDataPid) {
						std::from_chars_result r = std::from_chars(&attrValue.front(), &attrValue.back() + 1, pid);
						if (r.ec != std::errc{}) {
							spdlog::error("Could not parse passage id (integer number) from attribute string '{}': {}",
							    attrValue, std::make_error_code(r.ec).message());
						}

					} else if (attrName == attributePassageDataName) {
						name = attrValue;
					} else if (attrName == attributePassageDataTags) {
						tags = getFields(attrValue);
					} else if (attrName == passageMetadataKeys::position) {
						metadata.position = attrValue;
					} else if (attrName == passageMetadataKeys::size) {
						metadata.size = attrValue;
					} else if (attrName == passageMetadataKeys::sourceFile) {
						path = attrValue;
					}
				});

				if (pid == startNode) { s.metadata().twine2.start = name; }

				content = elementContent(elem);
			} else { // all other child nodes
				continue;
			}
			s.push_back({name, path, tags, std::move(content), metadata});
		}
		// TODO ? prepend(newPassage("StoryData", []string{}, string(s.marshalStoryData())))
	} else if (storyStoreNode) {
		// Twine 1 style story data chunk.
		/*
		    <div id="store-area" data-size="…" hidden>…</div>
		*/
		const auto& storyDataChildren = storyStoreNode->v.element.children;
		for (GumboIndex i = 0; i < storyDataChildren.length; ++i) {
			const GumboNode* child = static_cast<const GumboNode*>(storyDataChildren.data[i]);
			if (child->type != GUMBO_NODE_ELEMENT) { continue; }

			const GumboElement& elem = child->v.element;
			if (elem.tag != GUMBO_TAG_DIV || !findAttribute(elem, attributeTiddler)) { continue; }

			string name;
			std::filesystem::path path;
			std::vector<string> tags;
			Passage::Metadata metadata{};

			/*
			    <div tiddler="…" tags="…" created="…" modified="…" modifier="…" twine-position="…">…</div>
			*/
			forEachAttribute(elem, [&](std::string_view attrName, std::string_view attrValue) {
				if (attrName == attributeTiddler) {
					name = attrValue;
				} else if (attrName == attributePassageDataTags) {
					tags = getFields(attrValue);
				} else if (attrName == attributeTwinePosition) {
					metadata.position = attrValue;
				} else if (attrName == passageMetadataKeys::size) {
					metadata.size = attrValue;
				} else if (attrName == attributeTwineCreated) {
					std::from_chars_result r = std::from_chars(&attrValue.front(), &attrValue.back() + 1, metadata.created);
					if (r.ec != std::errc{}) {
						spdlog::error("Could not parse passage creation time (integer number) from attribute string '{}': {}",
						    attrValue, std::make_error_code(r.ec).message());
					}
				} else if (attrName == attributeTwineModified) {
					std::from_chars_result r = std::from_chars(&attrValue.front(), &attrValue.back() + 1, metadata.modified);
					if (r.ec != std::errc{}) {
						spdlog::error("Could not parse passage modification date (integer number) from attribute string '{}': {}",
						    attrValue, std::make_error_code(r.ec).message());
					}
				} else if (attrName == passageMetadataKeys::sourceFile) {
					path = attrValue;
				}
			});
			std::string content = elementContent(elem);
			if (!boost::algorithm::starts_with(content, "data:")) { content = tiddlerUnescaped(content); }
			s.push_back({name, path, tags, std::move(content), metadata});
		}
	} else {
		spdlog::error("Malformed HTML source; story data not found.");
	}

	return s;
}
