/*
    Copyright © 2020 ezsh. All rights reserved.
    Use of this source code is governed by a Simplified BSD License which
    can be found in the LICENSE file.
*/

#ifndef TWEEXX_FORMATS_HXX
#define TWEEXX_FORMATS_HXX

#include "./types.hxx"

#include <filesystem>
#include <map>
#include <stdexcept>
#include <string>
#include <string_view>
#include <vector>

#include "twee-export.h"

namespace twee {
	class TWEE_API StoryFormat {
	public:
		StoryFormat();
		StoryFormat(const std::string& id, const std::filesystem::path& filename, bool isTwine2);
		void loadMetadata();
		std::string source() const;

		const std::string& id() const
		{
			return id_;
		}

		const std::filesystem::path& fileName() const { return filename_; }

		const std::string& name() const
		{
			return name_;
		}

		bool isTwine2Style() const
		{
			return twine2_;
		}

		bool isTwine1Style() const
		{
			return !twine2_;
		}

		const std::string& versionStr() const
		{
			return version_;
		}

		bool proofing() const
		{
			return proofing_;
		}

	private:
		TWEE_NO_EXPORT void unmarshalMetadata();

		std::string id_;
		std::filesystem::path filename_;
		bool twine2_;
		std::string name_;
		std::string version_;
		bool proofing_;
	};


	class TWEE_API StoryFormatsMap {
	public:
		StoryFormatsMap(const std::vector<std::filesystem::path>& searchPaths);
		bool isEmpty() const;
		bool hasByID(string_view id) const;
		bool hasByTwine2Name(string_view name) const;
		bool hasByTwine2NameAndVersion(string_view name, string_view version) const;
		const StoryFormat& getByID(std::string_view id) const;
		const StoryFormat& getByTwine2Name(std::string_view name) const;
		const StoryFormat& getByTwine2NameAndVersion(string_view name, string_view version) const;
		std::vector<std::string> ids() const;

	private:
		std::string getIDByTwine2Name(std::string_view name) const;
		std::string getIDByTwine2NameAndVersion(std::string_view name, std::string_view version) const;
		std::map<std::string, StoryFormat, std::less<>> map_;
	};

	//----- exceptions ----
	class FormatNotFound: public std::runtime_error {
	public:
		FormatNotFound(const std::string& message);
	};

	class NoStoryFormatsFound: public std::runtime_error {
	public:
		NoStoryFormatsFound(const std::vector<std::filesystem::path>& searchDirs);
		std::vector<std::filesystem::path> searchDirerctories;
	};

	class FormatLoadingError: public std::runtime_error {
	public:
		FormatLoadingError(const StoryFormat& format, const std::string& message);
	};

} // namespace twee

#endif
