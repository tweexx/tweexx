/*
    Copyright © 2020 ezsh. All rights reserved.
    Use of this source code is governed by a Simplified BSD License which
    can be found in the LICENSE file.
*/

#include "./private_utils.hxx"

#include "../io.hxx"
#include "../passage.hxx"

#include <boost/algorithm/string/case_conv.hpp>

#include <map>
#include <sstream>
#include <string_view>
#include <utility>

namespace {
	using namespace std::string_view_literals;
	// "data:<mimeType>;base64,<data>
	constexpr const std::string_view base64Tag = ";base64,"sv;
	constexpr const std::string_view dataTag = "data:"sv;

	const std::map<std::string_view, std::string_view> knownFileExtensions = {
		// AUDIO
		{"aac"sv, "audio/aac"sv},
		{"flac"sv, "audio/flac"sv},
		{"ogg"sv, "audio/ogg"sv},
		{"wav"sv, "audio/wav"sv},
		// FONT NOTES:
		//
		// (ca. 2017) The IANA deprecated the various font subtypes of the
		// "application" type in favor of the new "font" type.  While the
		// standards were new at that point, many browsers had long accepted
		// such media types due to existing use in the wild—erroneous at
		// that point or not.
		//     otf   : application/font-sfnt  → font/otf
		//     ttf   : application/font-sfnt  → font/ttf
		//     woff  : application/font-woff  → font/woff
		//     woff2 : application/font-woff2 → font/woff2
		{"otf"sv, "font/otf"sv},
		{"ttf"sv, "font/ttf"sv},
		{"woff"sv, "font/woff"sv},
		{"woff2"sv, "font/woff2"sv},

		// IMAGE NOTES:
		{"gif"sv, "image/gif"sv},
		{"jpeg"sv, "image/jpeg"sv},
		{"jpg"sv, "image/jpeg"sv},
		{"png"sv, "image/png"sv},
		{"tiff"sv, "image/tiff"sv},
		{"tif"sv, "image/tiff"sv},
		{"webp"sv, "image/webp"sv},
		{"svg"sv, "image/svg+xml"sv},

		// METADATA NOTES:
		//
		// Name aside, WebVTT files are generic media cue metadata files
		// that may be used with either `<audio>` or `<video>` elements.
		// WebVTT (Web Video Text Tracks)
		{"vtt"sv, "text/vtt"sv},

		// VIDEO NOTES:
		{"mp4"sv, "video/mp4"sv},
		{"webm"sv, "video/webm"sv},
		{"ogv"sv, "video/ogg"sv}};
}

std::string twee::impl::toTitleCase(std::string s, const std::locale& loc)
{
	bool last = true;
	for (char& c: s) {
		c = last ? std::toupper(c, loc) : std::tolower(c, loc);
		last = std::isspace(c, loc);
	}
	return s;
}

std::string twee::impl::normalizedFileExt(const std::filesystem::path& filename)
{
	auto ext = filename.extension().string();
	if (ext.empty()) { return ext; }

	boost::algorithm::to_lower(ext);
	return ext.substr(1);
}

std::string_view twee::impl::mimeTypeFromFilename(const std::filesystem::path& filename)
{
	return mimeTypeFromExt(normalizedFileExt(filename));
}

std::string_view twee::impl::mimeTypeFromExt(std::string_view ext)
{
	const auto it = knownFileExtensions.find(ext);
	return it != knownFileExtensions.end() ? it->second : std::string_view();
}

std::string_view twee::impl::extForMimeType(std::string_view mimeType)
{
	for (const auto& p: knownFileExtensions) {
		if (p.second == mimeType) { return p.first; }
	}

	const auto slashPos = mimeType.find('/');
	if (slashPos != std::string_view::npos && mimeType.size() > slashPos + 1) { return mimeType.substr(slashPos + 1); }
	return "media";
}

std::string twee::impl::regexFromGlobPattern(const std::string& globPattern)
{
	std::ostringstream res;
	res << '^';
	for (std::size_t i = 0; i < globPattern.size(); ++i) {
		const auto c = globPattern[i];
		switch (c) {
			case '*':
				if (i + 1 < globPattern.size() && globPattern[i + 1] == '*') {
					res << ".*";
					++i;
				} else {
					res << "[^/]*";
				}
				break;
			case '.':
				res << "\\.";
				break;
			case '?':
				res << "[^/]";
				break;
			case '\\':
				res << "\\\\";
				break;
			case ':':
				res << "\\:";
				break;
			default:
				res << c;
				break;
		}
	}
	res << '$';
	return res.str();
}

bool twee::impl::isMediaPassage(const twee::Passage& passage)
{
	return passage.tagsHasAny({"Twine.image"sv, "Twine.audio"sv, "Twine.video"sv, "Twine.vtt"sv});
}

std::string_view twee::impl::mimeExt(const twee::Passage& passage)
{
	const auto mimeTypeEnd = passage.text().find(base64Tag);
	assert(mimeTypeEnd != std::string::npos);
	return twee::impl::extForMimeType(passage.text().substr(dataTag.length(), mimeTypeEnd - dataTag.length()));
}


void twee::impl::serializePassage(const twee::Passage& passage, std::ostream& os, twee::TweeVersion version, bool decode)
{
	const auto saveDecoded = [](const std::string& text, std::ostream& out) {
		const auto mimeTypeEnd = text.find(base64Tag);
		assert(mimeTypeEnd != std::string::npos);
		std::istringstream dataStream{text.substr(mimeTypeEnd + base64Tag.length())};
		twee::io::base64Decode(dataStream, out);
	};

	static const std::vector<std::string_view> rawContentTags{"script"sv, "stylesheet"sv};

	if (decode && isMediaPassage(passage)) {
		saveDecoded(passage.text(), os);
	} else if (passage.tagsHasAny(rawContentTags)) {
		os << passage.text();
	} else {
		passage.toTwee(os, version);
	}
}

std::string_view twee::impl::extForVersion(twee::TweeVersion version)
{
	switch (version) {
		case TweeVersion::Twee1:
			return ".tw"sv;
		case TweeVersion::Twee2:
			return ".tw2"sv;
		case TweeVersion::Twee3:
			return ".twee"sv;
	}
	std::unreachable();
}

// TODO consider C++20 lazy_split_view
std::vector<twee::string_view> twee::impl::split(twee::string_view str, string_view::value_type delimiter)
{
	std::vector<string_view> res;
	std::size_t lastOnePastDelimIndex = 0;

	for (std::size_t i = 0; i < str.size(); ++i) {
		if (str[i] == delimiter) {
			res.emplace_back(str.data() + lastOnePastDelimIndex, i - lastOnePastDelimIndex);
			lastOnePastDelimIndex = ++i;
		}
	}
	// tail
	if (lastOnePastDelimIndex < str.size()) {
		res.emplace_back(str.data() + lastOnePastDelimIndex, str.size() - lastOnePastDelimIndex);
	}

	return res;
}
