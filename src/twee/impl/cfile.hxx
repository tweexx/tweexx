/*
    Copyright © 2020 ezsh. All rights reserved.
    Use of this source code is governed by a Simplified BSD License which
    can be found in the LICENSE file.
*/

#ifndef TWEEXX_IMPL_CFILE_HXX
#define TWEEXX_IMPL_CFILE_HXX

#include <cstdio>
#include <filesystem>
#include <ios>
#include <system_error>

namespace io {
	class file {
	public:
		file(const std::filesystem::path& filename, std::ios_base::openmode modes);
		file() = default;
		file(file&& other) noexcept;
		~file();
		file& operator=(file&& other) noexcept;

		file(const file&) = delete;
		file& operator=(const file&) = delete;

		long size() const;

		bool eof() const noexcept
		{
			return std::feof(f_) != 0;
		}

		void seek(std::streamoff offset, std::ios_base::seekdir origin);

		std::size_t read(void* data, std::size_t size, std::error_code& ec);
		std::size_t read(void* data, std::size_t size);

		std::size_t write(const void* data, std::size_t size, std::error_code& ec);
		std::size_t write(const void* data, std::size_t size);

	private:
		static std::error_code& fail(std::error_code& ec);
		[[noreturn]] static void fail();
		void close();

		FILE* f_ = nullptr;
	};
} // namespace io
#if 0
    void
    fail(boost::json::error_code& ec)
    {
        ec.assign( errno, boost::json::generic_category() );
    }

public:
    ~file()
    {
        if(f_)
            std::fclose(f_);
    }


    file( char const* path, char const* mode )
    {
        open( path, mode );
    }

    file&
    operator=(file&& other) noexcept
    {
        close();
        f_ = other.f_;
        other.f_ = nullptr;
        return *this;
    }

    void
    close()
    {
        if(f_)
        {
            std::fclose(f_);
            f_ = nullptr;
            size_ = 0;
        }
    }



inline
std::string
read_file( char const* path, boost::json::error_code& ec )
{
    file f;
    f.open( path, "r", ec );
    if(ec)
        return {};
    std::string s;
    s.resize( f.size() );
    s.resize( f.read( &s[0], s.size(), ec) );
    if(ec)
        return {};
    return s;
}

inline
std::string
read_file( char const* path )
{
    boost::json::error_code ec;
    auto s = read_file( path, ec);
    if(ec)
        throw boost::json::system_error(ec);
    return s;
}
    };
}
#endif

#endif
