/*
    Copyright © 2020 ezsh. All rights reserved.
    Use of this source code is governed by a Simplified BSD License which
    can be found in the LICENSE file.
*/

#include "./twee2compat.hxx"

#include <regex>

namespace {
#ifdef GOLANG_REGEX
	std::regex twee2DetectRe{R"((?m)^:: *[^\[]*?(?: *\[.*?\])? *<(.*?)> *$)"};
	std::regex twee2HeaderRe{R"((?m)^(:: *[^\[]*?)( *\[.*?\])?(?: *<(.*?)>)? *$)"};
	std::regex twee2BadPosRe{R"((?m)^(::.*?) *{"position":" *"}$)"};
#endif
} // namespace

bool twee::impl::hasTwee2Syntax(const std::string& s)
{
	static const std::regex twee2DetectRe{R"(^:: *[^\[]*?(?: *\[.*?\])? *<(.*?)> *$)"};
	return std::regex_match(s, twee2DetectRe);
}

std::string twee::impl::toV3(const std::string& s)
{
	if (hasTwee2Syntax(s)) {
		static const std::regex twee2HeaderRe{R"(^(:: *[^\[]*?)( *\[.*?\])?(?: *<(.*?)>)? *$)"};
		static const std::regex twee2BadPosRe{R"(^(::.*?) *\{"position":" *"\}$)"};
		std::string r = std::regex_replace(s, twee2HeaderRe, R"(${1}${2} {"position":"${3}"})");
		r = std::regex_replace(r, twee2BadPosRe, "$1");
		return r;
	}
	return {};
}
