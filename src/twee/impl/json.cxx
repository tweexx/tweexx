/*
    Copyright © 2020 ezsh. All rights reserved.
    Use of this source code is governed by a Simplified BSD License which
    can be found in the LICENSE file.
*/

#include "./json.hxx"

#include <sstream>

twee::impl::json::JsonObject twee::impl::json::jsonFromString(const std::string& s)
{
	JsonObject res;
	std::istringstream is{s};
	readFromStream(is, res);
	return res;
}
