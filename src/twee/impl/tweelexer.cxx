/*
    Copyright © 2020 ezsh. All rights reserved.
    Use of this source code is governed by a Simplified BSD License which
    can be found in the LICENSE file.
*/

#include "./tweelexer.hxx"

#include "twee/utils.hxx"

/*
    Copyright © 2014–2019 Thomas Michael Edwards. All rights reserved.
    Use of this source code is governed by a Simplified BSD License which
    can be found in the LICENSE file.
*/

/*
    WARNING: Line Counts

    Ensuring proper line counts is fraught with peril as several methods
    modify the line count and it's entirely possible, if one is not careful,
    to count newlines multiple times.  For example, using `l.next()` to
    accept newlines, thus counting them, that are ultimately either emitted
    or ignored, which can cause them to be counted again.
*/
/*
    WARNING: Not Unicode Aware

    Twee syntax is strictly limited to US-ASCII, so there's no compelling
    reason to decode the UTF-8 input.
*/

#include <algorithm>
#include <iostream>
#include <stdexcept>
#include <string>
#include <utility>

namespace {
	const int eof = -1; // End of input value.

	const std::string headerDelim = "::";
	const std::string newlineHeaderDelim = "\n::";
} // namespace

std::ostream& twee::lexer::operator<<(std::ostream& os, const twee::lexer::Item& it)
{
	os << '[';
	switch (it.type) {
		case ItemType::EndOfInput:
			os << "EOF";
			break;
		case ItemType::Error:
			os << "Error";
			break;
		case ItemType::Header:
			os << "Header";
			break;
		case ItemType::Name:
			os << "Name";
			break;
		case ItemType::Tags:
			os << "Tags";
			break;
		case ItemType::Metadata:
			os << "Metadata";
			break;
		case ItemType::Content:
			os << "Content";
			break;
	}
	os << ": " << it.line << '/' << it.pos << ']';
	if (it.type == ItemType::EndOfInput) { return os; }
	os << ' ';
	if (it.type != ItemType::Error && it.val.size() > 80) {
		os << it.val.substr(0, 80) << "...";
	} else {
		os << it.val;
	}
	return os;
}

// stateFn state of the scanner as a function, which return the next state function.
// type stateFn func(*Tweelexer) stateFn

twee::lexer::TweeLexer::TweeLexer(const std::string_view& input, ItemChannel items)
	: input_{input}
	, items_{std::move(items)}
	, line_{1}
	, start_{0}
	, pos_{0}
{
}

// next returns the next byte, as a rune, in the input.
std::string::value_type twee::lexer::TweeLexer::next()
{
	if (pos_ >= input_.size()) { return eof; }
	auto r = input_[pos_];
	++pos_;
	if (r == '\n') { ++line_; }
	return r;
}

// peek returns the next byte, as a rune, in the input, but does not consume it.
std::string::value_type twee::lexer::TweeLexer::peek()
{
	if (pos_ >= input_.size()) { return eof; }
	return input_[pos_];
}

// emit sends an item to the item channel.
void twee::lexer::TweeLexer::emit(twee::lexer::ItemType t)
{
	items_(Item{t, static_cast<std::size_t>(line_), start_, std::string(&input_[start_], pos_ - start_)});
	// Some items may contain newlines that must be counted.
	if (t == ItemType::Content) { line_ += std::count(&input_[start_], &input_[pos_], '\n'); }
	start_ = pos_;
}

// backup rewinds our position in the input by one byte.
void twee::lexer::TweeLexer::backup()
{
	if (pos_ > start_) {
		--pos_;
		if (input_[pos_] == '\n') { --line_; }
	} else {
		throw std::runtime_error("backup would leave pos < start");
	}
}

// ignore skips over the pending input.
void twee::lexer::TweeLexer::ignore()
{
	line_ += std::count(&input_[start_], &input_[pos_], '\n');
	start_ = pos_;
}

// accept consumes the next byte if it's from the valid set.
bool twee::lexer::TweeLexer::accept(const std::string& valid)
{
	if (valid.find(next()) != std::string::npos) { return true; }
	backup();
	return false;
}

// acceptRun consumes a run of bytes from the valid set.
void twee::lexer::TweeLexer::acceptRun(const std::string& valid)
{
	std::string::value_type r;
	for (r = next(); valid.find(r) != std::string::npos; r = next()) {}
	if (r != eof) { backup(); }
}

// error() emits an error item and returns nil, allowing the scan to be terminated
// simply by returning the call to error.
twee::lexer::TweeLexer::Lexeme twee::lexer::TweeLexer::error(const std::string& message)
{
	items_(Item{ItemType::Error, static_cast<std::size_t>(line_), start_, message});
	return Lexeme::Nil;
}

bool twee::lexer::TweeLexer::startsWith(const std::string& s) const
{
	if (input_.size() - pos_ < s.size()) { return false; }
	return std::string_view(&input_[pos_], s.size()) == s;
}

// run runs the state machine for tweelexer.
void twee::lexer::TweeLexer::run()
{
	for (Lexeme state = Lexeme::Prolog; state != Lexeme::Nil;) { state = this->nextLexeme(state); }
}

twee::lexer::TweeLexer::Lexeme twee::lexer::TweeLexer::nextLexeme(TweeLexer::Lexeme lexeme)
{
	switch (lexeme) {
		case Lexeme::Nil:
			return Lexeme::Nil;
		case Lexeme::Prolog:
			return lexProlog();
		case Lexeme::Content:
			return lexContent();
		case Lexeme::HeaderDelim:
			return lexHeaderDelim();
		case Lexeme::Name:
			return lexName();
		case Lexeme::NextOptionalBlock:
			return lexNextOptionalBlock();
		case Lexeme::Tags:
			return lexTags();
		case Lexeme::Metadata:
			return lexMetadata();
	}
	std::unreachable();
}

#if 0
// GetItems returns the item channel.
// Called by the parser, not tweelexer.
func (l *Tweelexer) GetItems() chan Item {
	return l.items
}

// NextItem returns the next item and its ok status from the item channel.
// Called by the parser, not tweelexer.
func (l *Tweelexer) NextItem() (Item, bool) {
	// return <-l.items
	item, ok := <-l.items
	return item, ok
}

// Drain drains the item channel so the lexing goroutine will close the item channel and exit.
// Called by the parser, not tweelexer.
func (l *Tweelexer) Drain() {
	for range l.items {
	}
}
#endif

// acceptQuoted accepts a quoted string.
// The opening quote has already been seen.
void twee::lexer::TweeLexer::acceptQuoted(char quote, char escape)
{
	for (auto r = next(); r != '\n' && r != eof; r = next()) {
		if (r == quote) { return; }
		if (r == escape) {
			r = next();
			if (r != '\n' && r != eof) { continue; }
		}
	}
	error("unterminated quoted string");
}

// lexProlog skips until the first passage header delimiter.
twee::lexer::TweeLexer::Lexeme twee::lexer::TweeLexer::lexProlog()
{
	if (startsWith(headerDelim)) { return Lexeme::HeaderDelim; }
	if (auto i = curInput().find(newlineHeaderDelim.c_str(), 0, newlineHeaderDelim.size());
	    i != std::string_view::npos) {
		pos_ += i + 1;
		ignore();
		return Lexeme::HeaderDelim;
	}
	emit(ItemType::EndOfInput);
	return Lexeme::Nil;
}

// lexContent scans until a passage header delimiter.
twee::lexer::TweeLexer::Lexeme twee::lexer::TweeLexer::lexContent()
{
	if (startsWith(headerDelim)) { return Lexeme::HeaderDelim; }
	auto i = curInput().find(newlineHeaderDelim.c_str(), 0, newlineHeaderDelim.size());
	if (i != std::string_view::npos) {
		pos_ += i + 1;
		emit(ItemType::Content);
		return Lexeme::HeaderDelim;
	}
	pos_ = input_.size();
	if (pos_ > start_) { emit(ItemType::Content); }
	emit(ItemType::EndOfInput);
	return Lexeme::Nil;
}

// lexHeaderDelim scans a passage header delimiter.
twee::lexer::TweeLexer::Lexeme twee::lexer::TweeLexer::lexHeaderDelim()
{
	pos_ += headerDelim.size();
	emit(ItemType::Header);
	return Lexeme::Name;
}

// lexName scans a passage name until: one of the optional block delimiters, newline, or EOF.
twee::lexer::TweeLexer::Lexeme twee::lexer::TweeLexer::lexName()
{
	const auto skip = [this]() {
		for (int r = next();; r = next()) {
			switch (r) {
				case '\\':
					r = next();
					if (r != '\n' && r != eof) { break; }
					[[fallthrough]];
				case '[':
				case ']':
				case '{':
				case '}':
				case '\n':
				case eof:
					if (r != eof) {
						backup();
						return r;
					}
			}
		}
		return eof;
	};
	const int r = skip();
	// Always emit a name item, even if it's empty.
	emit(ItemType::Name);

	switch (r) {
		case '[':
			return Lexeme::Tags;
		case ']':
			error("unexpected right square bracket");
			return Lexeme::Nil;
		case '{':
			return Lexeme::Metadata;
		case '}':
			return error("unexpected right curly brace");
		case '\n':
			++pos_;
			ignore();
			return Lexeme::Content;
	}
	emit(ItemType::EndOfInput);
	return Lexeme::Nil;
}

// lexNextOptionalBlock scans within a header for the next optional block.
twee::lexer::TweeLexer::Lexeme twee::lexer::TweeLexer::lexNextOptionalBlock()
{
	// Consume space.
	acceptRun(" \t");
	ignore();

	auto r = peek();
	// panic(fmt.Sprintf("[lexNextOptionalBlock: %d, %d:%d]", l.line, l.start, l.pos))
	switch (r) {
		case '[':
			return Lexeme::Tags;
		case ']':
			return error("unexpected right square bracket");
		case '{':
			return Lexeme::Metadata;
		case '}':
			return error("unexpected right curly brace %#U");
		case '\n':
			++pos_;
			ignore();
			return Lexeme::Content;
		case eof:
			emit(ItemType::EndOfInput);
			return Lexeme::Nil;
	}
	return error(std::string("illegal character ") + static_cast<char>(r) + " amid the optional blocks");
}

// lexTags scans an optional tags block.
twee::lexer::TweeLexer::Lexeme twee::lexer::TweeLexer::lexTags()
{
	// Consume the left delimiter '['.
	++pos_;
	const auto skip = [this]() {
		for (;;) {
			auto r = next();
			switch (r) {
				case '\\':
					r = next();
					if (r != '\n' && r != eof) { break; }
					[[fallthrough]];
				case '\n':
				case eof:
					if (r == '\n') { backup(); }
					return error("unterminated tag block");
				case ']':
					return Lexeme::Tags;
				case '[':
					return error("unexpected left square bracket %#U");
				case '{':
					return error("unexpected left curly brace %#U");
				case '}':
					return error("unexpected right curly brace %#U");
			}
		}
	};
	skip();
	if (pos_ > start_) { emit(ItemType::Tags); }

	return Lexeme::NextOptionalBlock;
}

// lexMetadata scans an optional (JSON) metadata block.
twee::lexer::TweeLexer::Lexeme twee::lexer::TweeLexer::lexMetadata()
{
	// Consume the left delimiter '{'.
	++pos_;

	const auto skip = [this]() {
		int depth = 1;
		for (;;) {
			auto r = next();
			// switch r {
			// case '"': // Only double quoted strings are legal within JSON chunks.
			// 	if err := acceptQuoted(l, '"'); err != nil {
			// 		return l.errorf(err.Error())
			// 	}
			// case '\\':
			// 	r = l.next()
			// 	if r != '\n' && r != eof {
			// 		break
			// 	}
			// 	fallthrough
			// case '\n', eof:
			// 	if r == '\n' {
			// 		l.backup()
			// 	}
			// 	return l.errorf("unterminated metadata block")
			// case '{':
			// 	depth++
			// case '}':
			// 	depth--
			// 	switch {
			// 	case depth == 0:
			// 		break Loop
			// 	case depth < 0:
			// 		return l.errorf("unbalanced curly braces in metadata block")
			// 	}
			// }
			switch (r) {
				case '"': // Only double quoted strings are legal within JSON chunks.
					// 			if err := acceptQuoted(l, '"'); err != nil {
					// 				return l.errorf(err.Error())
					// 			}
					acceptQuoted('"');
					break;
				case '\n':
					backup();
					[[fallthrough]];
				case eof:
					error("unterminated metadata block");
					return;
				case '{':
					++depth;
					break;
				case '}':
					--depth;
					if (depth == 0) { return; }
			}
		}
	};
	skip();
	if (pos_ > start_) { emit(ItemType::Metadata); }

	return Lexeme::NextOptionalBlock;
}
