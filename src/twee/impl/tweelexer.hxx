/*
    Copyright © 2020 ezsh. All rights reserved.
    Use of this source code is governed by a Simplified BSD License which
    can be found in the LICENSE file.
*/

#pragma once

#include <cstddef>
#include <cstdint>
#include <functional>
#include <iosfwd>
#include <string>
#include <string_view>

namespace twee::lexer {
	// Item type constants.
	enum class ItemType: std::uint8_t {
		Error, // Error.  Its value is the error message.
		EndOfInput, // End of input.
		Header, // '::', but only when starting a line.
		Name, // Text w/ backslash escaped characters.
		Tags, // '[tag1 tag2 tagN]'.
		Metadata, // JSON chunk, '{…}'.
		Content //!< Plain text.
	};

	// Item represents a lexed item, a lexeme.
	struct Item {
		ItemType type; // Type of the item.
		std::size_t line; // Line within the input (1-base) of the item.
		std::size_t pos; // Starting position within the input, in bytes, of the item.
		std::string val; // Value of the item.
	};

	using ItemChannel = std::function<void(const Item& item)>;

	class TweeLexer {
	public:
		TweeLexer(const std::string_view& input, ItemChannel items);
		void run();

	private:
		enum class Lexeme: std::uint8_t {
			Nil,
			Prolog,
			Content,
			HeaderDelim,
			Name,
			NextOptionalBlock,
			Tags,
			Metadata,
		};

		std::string::value_type next();
		std::string::value_type peek();
		void backup();
		void ignore();
		bool accept(const std::string& valid);
		void acceptRun(const std::string& valid);
		void acceptQuoted(char quote, char escape = '\\');
		Lexeme error(const std::string& message);
		void emit(ItemType t);

		using stateFn = Lexeme (TweeLexer::*)();

		Lexeme nextLexeme(Lexeme lexeme);

		Lexeme lexProlog();
		Lexeme lexContent();
		Lexeme lexHeaderDelim();
		Lexeme lexName();
		Lexeme lexNextOptionalBlock();
		Lexeme lexTags();
		Lexeme lexMetadata();

		[[nodiscard]] bool startsWith(const std::string& s) const; // uni C++20

		[[nodiscard]] std::string_view curInput() const
		{
			return std::string_view{&input_[pos_], input_.size() - pos_};
		}

		std::string_view input_; // Byte slice being scanned.
		ItemChannel items_; // Channel of scanned items.
		std::ptrdiff_t line_; // Number of newlines seen (1-base).
		std::size_t start_; // Starting position of the current item.
		std::size_t pos_; // Current position within the input.
	};

	std::ostream& operator<<(std::ostream& os, const Item& it);

} // namespace twee::lexer
