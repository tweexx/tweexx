/*
    Copyright © 2020 ezsh. All rights reserved.
    Use of this source code is governed by a Simplified BSD License which
    can be found in the LICENSE file.
*/

/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TWEE_IMPL_SOURCES_SPLIT_HXX
#define TWEE_IMPL_SOURCES_SPLIT_HXX

#include "../exporter.hxx"

#include <filesystem>
#include <map>
#include <memory>
#include <string_view>
#include <vector>

namespace twee {
	class Passage;
}

namespace twee::impl {
	using string_view = std::basic_string_view<string::value_type>;

	class StringSplit {
	public:
		StringSplit(std::vector<string_view> split);
		std::size_t count() const { return split_.size(); }
		bool empty() const { return split_.empty(); }
		string_view operator[](std::size_t index) const { return split_[index]; }
		void merge(std::size_t index);

		std::filesystem::path toPath() const;

	private:
		std::vector<string_view> split_;
	};

	struct PassageEntry { // this is a file-like entry
		PassageEntry(const Passage* passage, std::vector<string_view>&& split);
		const Passage* passage;
		StringSplit split;
	};

	class PassageDir {
	public:
		void addEntry(PassageEntry pe);
		void ensureSubdir(const string& dir);

		void optmize(unsigned minDirSize);

		std::map<const Passage*, std::filesystem::path> flattern(
			const std::filesystem::path& rootDir, TweeVersion version) const;

		PassageDir& subdir(const string& dir);

	private:
		void addEntry(PassageEntry&& pe, std::size_t level);
		std::vector<PassageEntry> passages_;
		std::size_t level_ = 0;
		std::map<string, std::unique_ptr<PassageDir>> subdirs_;
	};


	PassageDir makeSplit(const std::vector<const Passage*>& passages, const twee::string& /*storyName*/, const SourceSplitOptions& opt);
}

#endif
