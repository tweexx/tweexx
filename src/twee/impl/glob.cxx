/*
    Copyright © 2020 ezsh. All rights reserved.
    Use of this source code is governed by a Simplified BSD License which
    can be found in the LICENSE file.
*/

#include "./glob.hxx"

#include "./private_utils.hxx"

#include <cassert>
#include <regex>

std::vector<std::filesystem::path> twee::impl::glob(std::filesystem::path pattern, bool includeDirs)
{
	pattern = pattern.lexically_normal();

	std::filesystem::path nonWildcardedPrefix = pattern.has_root_path() ? pattern.root_path() : std::filesystem::path();
	std::filesystem::path wildcard;
	std::filesystem::path relativePath = pattern.has_root_path() ? pattern.relative_path() : pattern;
	auto it = relativePath.begin();
	for (; it != relativePath.end(); ++it) {
		if (it->string().find_first_of("*?") ==
		    std::basic_string<std::filesystem::path::value_type>::npos) { // include other symbols?
			nonWildcardedPrefix /= *it;
		} else {
			wildcard /= *it;
			++it;
			break;
		}
	}
	// the rest to wildcard
	for (; it != relativePath.end(); ++it) { wildcard /= *it; }

	std::string regexStr = regexFromGlobPattern(wildcard.string());
	std::regex rx{regexStr};

	std::vector<std::filesystem::path> res;
	for (const auto& p: std::filesystem::recursive_directory_iterator(nonWildcardedPrefix)) {
		if (!includeDirs && std::filesystem::is_directory(p)) { continue; }
		if (std::regex_match(p.path().string(), rx)) {
			assert(std::filesystem::exists(p.path()));
			res.push_back(p.path());
		}
	}
	return res;
}
