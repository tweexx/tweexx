/*
    Copyright © 2020 ezsh. All rights reserved.
    Use of this source code is governed by a Simplified BSD License which
    can be found in the LICENSE file.
*/

#ifndef TWEE_IMPL_JSON_HXX
#define TWEE_IMPL_JSON_HXX

#include "twee-config.h"

#ifdef JsonCpp_FOUND
#	include <json/json.h>
#	include <json/writer.h>
#else
#	include <boost/property_tree/json_parser.hpp>
#	include <boost/property_tree/ptree.hpp>
#endif

#include <iosfwd>

namespace twee::impl::json {
#ifdef JsonCpp_FOUND
	using JsonObject = Json::Value;

	template <class Value>
	inline void putValue(JsonObject& obj, const char* name, Value val) {
		obj[name] = val;
	}

	inline std::string getValue(const JsonObject& obj, const char* name, const std::string& defaultValue) {
		return obj.get(name, defaultValue).asString();
	}

	inline int getValue(const JsonObject& obj, const char* name, int defaultValue) {
		return obj.get(name, defaultValue).asInt();
	}

	inline bool getValue(const JsonObject& obj, const char* name, bool defaultValue) {
		return obj.get(name, defaultValue).asBool();
	}

	inline double getValue(const JsonObject& obj, const char* name, double defaultValue) {
		return obj.get(name, defaultValue).asDouble();
	}

	inline JsonObject getValue(const JsonObject& obj, const char* name, const JsonObject& defaultValue) {
		return obj.get(name, defaultValue);
	}

	template <class F>
	inline void forEachMember(const JsonObject& obj, F f) {
		const auto members = obj.getMemberNames();
		for (const auto& mn: members) {
			f(mn, obj[mn]);
		}
	}

	inline std::string objValueAsString(const JsonObject& obj) {
		return obj.asString();
	}

	inline void writeToStream(std::ostream& os, const JsonObject& obj) {
		Json::StreamWriterBuilder builder;
		builder["enableYAMLCompatibility"] = true;
		builder["precision"] = 12;
		std::unique_ptr<Json::StreamWriter> writer(builder.newStreamWriter());
		writer->write(obj, &os);
	}

	inline void readFromStream(std::istream& is, JsonObject& obj) {
		is >> obj;
	}
#else
	using JsonObject = boost::property_tree::ptree;

	template <class Value>
	inline void putValue(JsonObject& obj, const char* name, const Value& val) {
		obj.put(name, val);
	}

	template <>
	inline void putValue(JsonObject& obj, const char* name, const JsonObject& val) {
		obj.put_child(name, val);
	}

	template <class Value>
	inline Value getValue(const JsonObject& obj, const char* name, Value defaultValue) {
		return obj.get<Value>(name, defaultValue);
	}

	inline JsonObject getValue(const JsonObject& obj, const char* name, const JsonObject& defaultValue) {
		return obj.get_child(name, defaultValue);
	}

	template <class F>
	inline void forEachMember(const JsonObject& obj, F f) {
		for (const auto& p: obj) {
			f(p.first, p.second);
		}
	}

	inline std::string objValueAsString(const JsonObject& obj) {
		return obj.get_value<std::string>();
	}

	inline void writeToStream(std::ostream& os, const JsonObject& obj) {
		boost::property_tree::write_json(os, obj);
	}

	inline void readFromStream(std::istream& is, JsonObject& obj) {
		boost::property_tree::read_json(is, obj);
	}
#endif
	JsonObject jsonFromString(const std::string& s);
} // namespace twee::impl

#endif
