/*
    Copyright © 2020 ezsh. All rights reserved.
    Use of this source code is governed by a Simplified BSD License which
    can be found in the LICENSE file.
*/

#include "./escaping.hxx"

#include <boost/lexical_cast.hpp>

#include <iostream>

#include "cpp-slugify/slugify.hpp"

void twee::escapers::attribute(std::ostream& os, const std::string_view s)
{
	for (auto c: s) {
		switch (c) {
			case '&':
				os << "&amp;";
				break;
			case '\"':
				os << "&quot;";
				break;
			case '\'':
				os << "&apos;";
				break;
			default:
				os << c;
				break;
		}
	}
}

// Escape the minimum characters required for general HTML escaping—i.e. only
// the special characters (`&`, `<`, `>`, `"`, `'`).
//
// NOTE: Converts to entities (`&quot;`) rather than to numeric character reference (`&#34;`).
// While browsers will happily accept the NCRs, a not insignificant amount of JavaScript
// code does not expect it and will fail to properly unescape the NCR—expecting
// only `&quot;`.
//
// The primary special characters (`&`, `<`, `>`, `"`) should always be
// converted to their entity forms and never to an NCR form.  Saving one byte
// (5 vs. 6) is not worth the issues it causes.

void twee::escapers::html(std::ostream& os, const std::string_view s)
{
	for (auto c: s) {
		switch (c) {
			case '&':
				os << "&amp;";
				break;
			case '\"':
				os << "&quot;";
				break;
			case '\'':
				os << "&apos;";
				break;
			case '<':
				os << "&lt;";
				break;
			case '>':
				os << "&gt;";
				break;
			default:
				os << c;
				break;
		}
	}
}

void twee::escapers::tiddler(std::ostream& os, const std::string_view s)
{
	for (auto c: s) {
		switch (c) {
			case '&':
				os << "&amp;";
				break;
			case '\"':
				os << "&quot;";
				break;
#if 1
			case '\'':
				os << "&#39;";
				break;
#endif
			case '<':
				os << "&lt;";
				break;
			case '>':
				os << "&gt;";
				break;
#if 1
			case '\\':
				os << "\\s";
				break;
			case '\t':
				os << "\\t";
				break;
			case '\n':
				os << "\\n";
				break;
#endif
			default:
				os << c;
				break;
		}
	}
}

void twee::unescapers::tiddler(std::ostream& os, const std::string_view s)
{
	if (s.empty()) { return; }
	// NOTE: We only need the newline, tab, and backslash escapes here since
	// `tiddlerUnescaper()` is only used when loading Twine 1 HTML and
	// Gumbo already handles entity/reference unescaping for us.
	const char* ptr = s.data();
	std::size_t i = 0;
	for (; i < s.size() - 1; ++i, ++ptr) {
		if (*ptr != '\\') {
			os << *ptr;
		} else {
			switch (ptr[1]) { // safe because we iterate to one before last s character
				case 'n':
					os << '\n';
					break;
				case 't':
					os << '\t';
					break;
				case 's':
					os << '\\';
					break;
				default:
					continue;
			}
			++i;
			++ptr;
		}
	}
	if (i < s.size()) { os << *ptr; }
}

std::string twee::escaped(const std::string& s, impl::string_escaper_func f)
{
	return boost::lexical_cast<std::string>(impl::escaper(f, s));
}

std::string twee::escaped(std::string_view s, impl::string_escaper_func f)
{
	return boost::lexical_cast<std::string>(impl::escaper(f, s));
}

std::string twee::tiddlerUnescaped(const std::string& s)
{
	return escaped(s, &unescapers::tiddler);
}


/*
    Twee escaping/unescaping utilities.
*/

// Encode set: '\\', '[', ']', '{', '}'.
void twee::escapers::twee(std::ostream& os, const std::string_view s)
{
	for (auto c: s) {
		switch (c) {
			case '\\':
				os << "\\\\";
				break;
			case '[':
				os << "\\[";
				break;
			case ']':
				os << "\\]";
				break;
			case '{':
				os << "\\{";
				break;
			case '}':
				os << "\\}";
				break;
			default:
				os << c;
				break;
		}
	}
}

void twee::unescapers::twee(std::ostream& os, const std::string_view s)
{
	if (s.empty()) { return; }

	const char* ptr = s.data();
	std::size_t i = 0;
	for (; i < s.size() - 1; ++i, ++ptr) {
		if (*ptr != '\\') {
			os << *ptr;
		} else {
			switch (ptr[1]) { // safe because we iterate to one before last s character
				case '\\':
				case '[':
				case ']':
				case '{':
				case '}':
					os << ptr[1];
					break;
				default:
					continue;
			}
			++i;
			++ptr;
		}
	}
	if (i < s.size()) { os << *ptr; }
}


std::string twee::tweeEscape(const std::string& s)
{
	return escaped(s, escapers::twee);
}

std::string twee::tweeUnescape(const std::string& s)
{
	return escaped(s, unescapers::twee);
}

std::string twee::attrEscaped(const std::string& s)
{
	return escaped(s, escapers::attribute);
}

std::string twee::attrEscaped(std::string_view s)
{
	return escaped(s, escapers::attribute);
}

std::string twee::htmlEscaped(const std::string& s)
{
	return escaped(s, escapers::html);
}

std::string twee::slugify(std::string input)
{
	// the function is in a header file and not declared inline :(
	// leads to multiple definition error
	return ::slugify(input);
}
