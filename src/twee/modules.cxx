/*
    Copyright © 2020 ezsh. All rights reserved.
    Use of this source code is governed by a Simplified BSD License which
    can be found in the LICENSE file.
*/

#include "./modules.hxx"

#include "./escaping.hxx"
#include "./impl/private_utils.hxx"
#include "./io.hxx"

#include <boost/algorithm/string/trim.hpp>
#include <fmt/core.h>
#include <fmt/ostream.h>
#include <spdlog/spdlog.h>

#include <iomanip>

namespace {
	std::filesystem::path absoultePath(const std::filesystem::path& p, const std::filesystem::path& baseDir)
	{
		return p.has_root_path() ? p : (baseDir / p).lexically_normal();
	}

	bool loadModuleTagged(
		std::ostream& os, std::string_view tag, const std::filesystem::path& filename,
		const std::filesystem::path& baseDir, std::string_view encoding)
	{
		std::string source = twee::io::readAllWithEncoding(absoultePath(filename, baseDir), encoding);

		boost::algorithm::trim(source);
		if (source.empty()) { return false; }

		const std::string idSlug = fmt::format("{}-module-{}", tag, twee::slugify(filename.stem().string()));
		std::string mimeType;

		if (tag == "script") {
			mimeType = "text/javascript";
		} else if (tag == "style") {
			mimeType = "text/css";
		}

		os << "<" << tag << " id=" << std::quoted(idSlug) << " type=" << std::quoted(mimeType)
		   << " data-sourcefile=" << filename << '>' << source << "</" << tag << '>';
		return true;
	}

	bool loadModuleFont(std::ostream& os, const std::filesystem::path& filename, const std::filesystem::path& baseDir)
	{
		const auto source = twee::io::readAllAsBase64(absoultePath(filename, baseDir));
		const auto family = filename.stem().string();
		const auto ext = twee::impl::normalizedFileExt(filename);
		const auto mediaType = twee::impl::mimeTypeFromExt(ext);
		const std::string idSlug = "style-module-" + twee::slugify(family);
		std::string hint;
		if (ext == "ttf") {
			hint = "truetype";
		} else if (ext == "otf") {
			hint = "opentype";
		} else {
			hint = ext;
		}

		os << "<style id=" << std::quoted(idSlug)
		   << " type=\"text/css\">@font-face {\n\tfont-family: " << std::quoted(family)
		   << ";\n\tsrc: url(\"data:" << mediaType << ";base64," << source << "\") format(" << std::quoted(hint)
		   << ");\n}</style>";
		return true;
	}

} // namespace

bool twee::loadModule(
	std::ostream& os, const std::filesystem::path& filename, const std::filesystem::path& baseDir,
	std::string_view encoding)
{
	using namespace std::string_view_literals;
	const auto ext = twee::impl::normalizedFileExt(filename);
	if (ext == "css"sv) { return loadModuleTagged(os, "style"sv, filename, baseDir, encoding); }
	if (ext == "js"sv) { return loadModuleTagged(os, "script"sv, filename, baseDir, encoding); }
	if (ext == "otf"sv || ext == "ttf"sv || ext == "woff"sv || ext == "woff2"sv) {
		return loadModuleFont(os, filename, baseDir);
	}

	spdlog::trace("Ignoring module file {}: don't know how to handle module type '{}'", fmt::streamed(filename), ext);
	return false;
}
