/*
    Copyright © 2020 ezsh. All rights reserved.
    Use of this source code is governed by a Simplified BSD License which
    can be found in the LICENSE file.
*/

/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TWEEXX_ESCAPING_HXX
#define TWEEXX_ESCAPING_HXX

#include <iosfwd>
#include <string>
#include <string_view>

namespace twee {
	namespace escapers {
		void attribute(std::ostream& os, const std::string_view s);
		void html(std::ostream& os, const std::string_view s);
		void tiddler(std::ostream& os, const std::string_view s);
		void twee(std::ostream& os, const std::string_view s);
	} // namespace escapers

	namespace unescapers {
		void tiddler(std::ostream& os, const std::string_view s);
		void twee(std::ostream& os, const std::string_view s);
	} // namespace unescapers

	namespace impl {
		using string_escaper_func = void (*)(std::ostream&, const std::string_view);

		template<class String, class F>
		class escaper {
		public:
			escaper(F func, const String& s)
			    : func_{func}
			    , s_{s}
			{
			}

			void out(std::ostream& os) const
			{
				func_(os, s_);
			}

		private:
			escaper& operator=(const escaper&) = delete;

			F func_;
			const String& s_;
		};

		template <class F>
		class escaper<std::string_view, F> {
		public:
			escaper(F func, std::string_view& s)
			    : func_{func}
			    , s_{std::move(s)}
			{
			}

			void out(std::ostream& os) const
			{
				func_(os, s_);
			}

		private:
			escaper& operator=(const escaper&) = delete;

			F func_;
			std::string_view s_;
		};

		template<class F, class String>
		inline std::ostream& operator<<(std::ostream& os, const escaper<F, String>& e)
		{
			e.out(os);
			return os;
		}
	} // namespace impl


	std::string escaped(const std::string& s, impl::string_escaper_func f);
	std::string escaped(std::string_view s, impl::string_escaper_func f);

	auto inline attrEscape(const std::string& s)
	{
		return impl::escaper(&escapers::attribute, s);
	}
	std::string attrEscaped(const std::string& s);
	std::string attrEscaped(std::string_view s);

	template <std::size_t N>
	std::string attrEscaped(const char (&s)[N])
	{
		return attrEscaped(std::string_view(s, N - 1));
	}

	auto inline htmlEscape(const std::string& s)
	{
		return impl::escaper(&escapers::html, s);
	}
	auto inline tiddlerEscape(const std::string& s)
	{
		return impl::escaper(&escapers::tiddler, s);
	}

	std::string htmlEscaped(const std::string& s);

	auto inline tiddlerUnescape(const std::string& s)
	{
		return impl::escaper(&unescapers::tiddler, s);
	}

	std::string tiddlerUnescaped(const std::string& s);

	std::string tweeEscape(const std::string& s);
	std::string tweeUnescape(const std::string& s);

	std::string slugify(std::string s);
} // namespace twee

#endif
