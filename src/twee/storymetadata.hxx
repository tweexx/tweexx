/*
    Copyright © 2020 ezsh. All rights reserved.
    Use of this source code is governed by a Simplified BSD License which
    can be found in the LICENSE file.
*/

#ifndef TWEEXX_STORYDATA_HXX
#define TWEEXX_STORYDATA_HXX

#include <map>
#include <set>
#include <string>

namespace twee {

	class Passage;

	struct StoryMetadata {
		using Twine1Settings = std::map<std::string, std::string>;
		using Twine2Options = std::set<std::string>;
		using Twine2TagColors = std::map<std::string, std::string>;

		std::string ifid; // Story IFID.

		// WARNING: Do not use individual fields here as story formats are allowed to
		// define their own `StorySettings` pairs—via a custom header file—so we have
		// no way of knowing what the keys might be prior to parsing the passage.
		struct {
			std::string legacyIFID;
			Twine1Settings settings;
		} twine1;

		struct {
			std::string format; // Name of the story format.
			std::string formatVersion; // SemVer of the story format.
			Twine2Options options; // Map of option-name/bool pairs.
			std::string start; // Name of the starting passage.
			Twine2TagColors tagColors; // Unused by Tweego.  Map of tag-name/color pairs.
			double zoom = 1.; // Unused by Tweego.  Zoom level.  Why is this even a part of the story metadata?  It's
			                  // editor configuration.
		} twine2;
	};

	void loadStoryData(StoryMetadata& metadata, const Passage& storyData);
	Passage createStoryDataPassage(const StoryMetadata& metadata);
} // namespace twee

#endif
