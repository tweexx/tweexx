/*
    Copyright © 2020 ezsh. All rights reserved.
    Use of this source code is governed by a Simplified BSD License which
    can be found in the LICENSE file.
*/

#ifndef TWEEXX_COMPILER_HXX
#define TWEEXX_COMPILER_HXX

#include "./types.hxx"

#include <filesystem>
#include <iosfwd>

#include "twee-export.h"

namespace twee {

	class Story;
	class StoryFormat;
	class StoryFormatsMap;

	struct StoryCompilationOptions {
		string startingPassage;
		string storyFormatId;
		std::filesystem::path headFile;
	};

	class TWEE_API Compiler {
	public:
		Compiler(const StoryFormatsMap& formats, const StoryCompilationOptions& options);

		void toTwine2Archive(std::ostream& os, const Story& story) const;
		void toTwine1Archive(std::ostream& os, const Story& story) const;

		/**
		 * @brief Generate story HTML
		 *
		 * Twine1 or Twine2 HTML style will be selected basing on the story format
		 * @param os stream to write the story HTML to
		 * @param formats available formats
		 * @param options generation options
		 */
		void toHTML(std::ostream& os, const Story& story) const;
		void toTwine2HTML(std::ostream& os, const Story& story) const;
		void toTwine1HTML(std::ostream& os, const Story& story) const;

	private:
		TWEE_NO_EXPORT const StoryFormat& selectFormat(const Story& story) const;
		TWEE_NO_EXPORT const string& startPassageName(const Story& story) const;
		TWEE_NO_EXPORT void injectHeadContent(std::ostream& os, const Story& story) const;
		TWEE_NO_EXPORT void getTwine2DataChunk(
			std::ostream& os, const Story& story, const StoryFormat& format, const string& startingPassage) const;
		TWEE_NO_EXPORT void getTwine1PassageChunk(std::ostream& os, const Story& story) const;

		TWEE_NO_EXPORT std::size_t twine1PublicPassagesCount(const Story& story) const;

		const StoryCompilationOptions& options_;
		const StoryFormatsMap& formats_;
	};
} // namespace twee

#endif
