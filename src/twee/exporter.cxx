/*
    Copyright © 2020 ezsh. All rights reserved.
    Use of this source code is governed by a Simplified BSD License which
    can be found in the LICENSE file.
*/

#include "./exporter.hxx"

#include "./escaping.hxx"
#include "./impl/private_utils.hxx"
#include "./impl/sources_split.hxx"
#include "./story.hxx"
#include "./storymetadata.hxx"

#include <algorithm>
#include <cassert>
#include <fstream>
#include <memory>

void twee::storyToTwee(std::ostream& os, const twee::Story& story, twee::TweeVersion version)
{
	story.createStoryTitlePassage().toTwee(os, version);
	createStoryDataPassage(story.metadata()).toTwee(os, version);

	for (const auto& passage: story.passages()) { passage.toTwee(os, version); }
}

void twee::storyToTwee(
	const std::filesystem::path& dir, const twee::Story& story, const SourceSplitOptions& splitOptions,
	twee::TweeVersion version)
{
	assert(std::filesystem::is_directory(dir)); // caller must ensure this

	const std::string baseName = slugify(story.name());

	std::vector<const Passage*> passages;
	passages.reserve(story.passages().size() + 2);
	const Passage storyTitlePassage = story.createStoryTitlePassage();
	passages.push_back(&storyTitlePassage);
	const Passage storyDataPassage = createStoryDataPassage(story.metadata());
	passages.push_back(&storyDataPassage);

	std::ranges::transform(
		story.passages(), std::back_inserter(passages), [](const Passage& p) { return std::addressof(p); });

	impl::PassageDir split = impl::makeSplit(passages, baseName, splitOptions);
	split.optmize(splitOptions.minGroupSize());

	for (const auto& pp: split.flattern(dir, version)) {
		std::filesystem::create_directories(pp.second.parent_path());
		std::ofstream out{pp.second};
		out.exceptions(std::ios_base::failbit | std::ios_base::badbit);
		impl::serializePassage(*pp.first, out, version, splitOptions.extractMedia());
	}
}

// ------ twee::SourceSplitOptions --------

twee::SourceSplitOptions::SourceSplitOptions(bool extractMedia, bool split)
	: maxDepth_{-1}
	, delimiterRegex_{"\\s+"}
	, minFragmentLength_{3}
	, minGroupSize_{2}
	, extractMedia_{extractMedia}
	, split_{split}
	, preserveSourceInfo_{true}
{
}

bool twee::SourceSplitOptions::extractMedia() const
{
	return extractMedia_;
}

bool twee::SourceSplitOptions::split() const
{
	return split_;
}

int twee::SourceSplitOptions::maxDepth() const
{
	return maxDepth_;
}

twee::string twee::SourceSplitOptions::delimiterRegex() const
{
	return delimiterRegex_;
}

unsigned twee::SourceSplitOptions::minFragmentLength() const
{
	return minFragmentLength_;
}

unsigned twee::SourceSplitOptions::minGroupSize() const
{
	return minGroupSize_;
}

bool twee::SourceSplitOptions::preserveSourceInfo() const
{
	return preserveSourceInfo_;
}

twee::SourceSplitOptions::ThisType& twee::SourceSplitOptions::extractMedia(bool value)
{
	extractMedia_ = value;
	return *this;
}

twee::SourceSplitOptions::ThisType& twee::SourceSplitOptions::split(bool value)
{
	split_ = value;
	return *this;
}

twee::SourceSplitOptions::ThisType& twee::SourceSplitOptions::delimiterRegex(const twee::string& value)
{
	delimiterRegex_ = value;
	return *this;
}

twee::SourceSplitOptions::ThisType& twee::SourceSplitOptions::maxDepth(int value)
{
	maxDepth_ = value;
	return *this;
}

twee::SourceSplitOptions::ThisType& twee::SourceSplitOptions::minFragmentLength(unsigned int value)
{
	minFragmentLength_ = value;
	return *this;
}

twee::SourceSplitOptions::ThisType& twee::SourceSplitOptions::minGroupSize(unsigned int value)
{
	minGroupSize_ = value;
	return *this;
}

twee::SourceSplitOptions::ThisType& twee::SourceSplitOptions::preserveSourceInfo(bool value)
{
	preserveSourceInfo_ = value;
	return *this;
}
