/*
    Copyright © 2020 ezsh. All rights reserved.
    Use of this source code is governed by a Simplified BSD License which
    can be found in the LICENSE file.
*/

#ifndef TWEEXX_MODULES_HXX
#define TWEEXX_MODULES_HXX

#include <filesystem>
#include <iosfwd>
#include <string_view>

namespace twee {
	bool loadModule(
		std::ostream& os, const std::filesystem::path& filename, const std::filesystem::path& baseDir,
		std::string_view encoding);
}

#endif
