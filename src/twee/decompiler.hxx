/*
    Copyright © 2020 ezsh. All rights reserved.
    Use of this source code is governed by a Simplified BSD License which
    can be found in the LICENSE file.
*/

#ifndef TWEEXX_DECOMPILER_HXX
#define TWEEXX_DECOMPILER_HXX

#include "./types.hxx"

#include <filesystem>

#include "twee-export.h"

namespace twee {
	class Story;

	TWEE_API Story decompileHTML(const std::filesystem::path& filename, string_view encoding);
}

#endif
