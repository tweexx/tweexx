/*
    Copyright © 2020 ezsh. All rights reserved.
    Use of this source code is governed by a Simplified BSD License which
    can be found in the LICENSE file.
*/

#include "./io.hxx"

#include "./impl/cfile.hxx"

#include "twee-config.h"

#include <array>
#include <fstream>
#include <memory>
#include <sstream>
#include <stdexcept>
#include <vector>

namespace {
	using namespace std::literals;
	const constexpr std::string_view base64_encode_table = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"sv;
}

// std::string decode64(const std::string &val) {
//     using namespace boost::archive::iterators;
//     using It = transform_width<binary_from_base64<std::string::const_iterator>, 8, 6>;
//     return boost::algorithm::trim_right_copy_if(std::string(It(std::begin(val)), It(std::end(val))), [](char c) {
//         return c == '\0';
//     });
// }
//
// std::string encode64(const std::string &val) {
//     using namespace boost::archive::iterators;
//     using It = base64_from_binary<transform_width<std::string::const_iterator, 6, 8>>;
//     auto tmp = std::string(It(std::begin(val)), It(std::end(val)));
//     return tmp.append((3 - val.size() % 3) % 3, '=');
// }

#ifdef Iconv_FOUND
#	include <iconv.hpp>
namespace {
	std::string convertToUTF8(std::string_view s, twee::string_view enc)
	{
		iconvpp::converter converter{"utf-8", twee::string(enc)};
		std::string res;
		converter.convert(s, res);
		return res;
	}
} // namespace
#else
#	include <boost/locale.hpp>
namespace {
	std::string convertToUTF8(const std::string& s, const std::string& enc)
	{
		return boost::locale::conv::to_utf<char>(s.c_str(), enc);
	}
} // namespace
#endif

std::string twee::io::readAll(const std::filesystem::path& fileName)
{
	::io::file fp{fileName, std::ios_base::in | std::ios_base::binary};

	std::array<unsigned char, 3> utf8Bom;
	const auto bomsize = fp.read(utf8Bom.data(), utf8Bom.size());
	const long offset = (bomsize == 3 && utf8Bom[0] == 0xEF && utf8Bom[1] == 0xBB && utf8Bom[2] == 0xBF) ? 3 : 0;
	std::string contents;
	contents.resize(static_cast<std::size_t>(fp.size() - offset));
	if (offset != sizeof(utf8Bom)) {
		fp.seek(offset, std::ios_base::beg);
	}
	const auto contentSize = fp.read(contents.data(), contents.size());
	if (contentSize != contents.size()) { throw std::runtime_error("Can not read from file " + fileName.string()); }
	return contents;
}

std::string twee::io::readAllWithEncoding(const std::filesystem::path& fileName, string_view enc)
{
	if (enc == "utf-8" || enc == "UTF-8") { return readAll(fileName); }
	return convertToUTF8(readAll(fileName), enc);
}

std::string twee::io::readAllAsBase64(const std::filesystem::path& fileName)
{
	std::ifstream inp{fileName};
	std::ostringstream os;

	base64Encode(inp, os);
	return std::move(os).str();
}

void twee::io::writeAll(const std::filesystem::path& fileName, std::string_view data)
{
	::io::file fp{fileName, std::ios_base::out | std::ios_base::binary};
	fp.write(data.data(), data.size());
}

#if 0
func fileReadAllAsBase64(filename string) ([]byte, error) {
	var (
		r    io.Reader
		data []byte
		err  error
	)
	if filename == "-" {
		r = os.Stdin
	} else {
		var f *os.File
		if f, err = os.Open(filename); err != nil {
			return nil, err
		}
		defer f.Close()
		r = f
	}
	if data, err = ioutil.ReadAll(r); err != nil {
		return nil, err
	}
	buf := make([]byte, base64.StdEncoding.EncodedLen(len(data))) // try to avoid additional allocations
	base64.StdEncoding.Encode(buf, data)
	return buf, nil
}

func fileReadAllAsUTF8(filename string) ([]byte, error) {
	return fileReadAllWithEncoding(filename, "utf-8")
}

func fileReadAllWithEncoding(filename, encoding string) ([]byte, error) {
	var (
		r      io.Reader
		data   []byte
		rsLF   = []byte(recordSeparatorLF)
		rsCRLF = []byte(recordSeparatorCRLF)
		rsCR   = []byte(recordSeparatorCR)
		err    error
	)

	// Read in the entire file.
	if filename == "-" {
		r = os.Stdin
	} else {
		var f *os.File
		if f, err = os.Open(filename); err != nil {
			return nil, err
		}
		defer f.Close()
		r = f
	}
	if data, err = ioutil.ReadAll(r); err != nil {
		return nil, err
	}

	// Convert the charset to UTF-8, if necessary.
	encoding = charset.NormalizedName(encoding)
	if utf8.Valid(data) {
		switch encoding {
		case "", "utf-8", "utf8", "ascii", "us-ascii":
			// no-op
		default:
			log.Printf("warning: read %s: Already valid UTF-8; skipping charset conversion.", filename)
		}
	} else {
		switch encoding {
		case "utf-8", "utf8", "ascii", "us-ascii":
			log.Printf("warning: read %s: Invalid UTF-8; assuming charset is %s.", filename, fallbackCharset)
			fallthrough
		case "":
			encoding = charset.NormalizedName(fallbackCharset)
		}
		if r, err = charset.NewReader(encoding, bytes.NewReader(data)); err != nil {
			return nil, err
		}
		if data, err = ioutil.ReadAll(r); err != nil {
			return nil, err
		}
		if !utf8.Valid(data) {
			return nil, fmt.Errorf("read %s: Charset conversion yielded invalid UTF-8.", filename)
		}
	}

	// Strip the UTF BOM (\uFEFF), if it exists.
	if bytes.Equal(data[:3], []byte(utfBOM)) {
		data = data[3:]
	}

	// Normalize record separators.
	data = bytes.Replace(data, rsCRLF, rsLF, -1)
	data = bytes.Replace(data, rsCR, rsLF, -1)

	return data, nil
}

func alignRecordSeparators(data []byte) []byte {
	switch runtime.GOOS {
	case "windows":
		return bytes.Replace(data, []byte(recordSeparatorLF), []byte(recordSeparatorCRLF), -1)
	default:
		return data
	}
}

#endif

void twee::io::base64Encode(std::istream& is, std::ostream& os)
{
	std::size_t counter = 0;
	int val = 0, valb = -6;
	for (auto c = is.get(); !is.eof(); c = is.get()) {
		val = (val << 8) + c;
		valb += 8;
		while (valb >= 0) {
			os.put(base64_encode_table[(val >> valb) & 0x3F]);
			++counter;
			valb -= 6;
		}
	}
	if (valb > -6) {
		os.put(base64_encode_table[((val << 8) >> (valb + 8)) & 0x3F]);
		++counter;
	}
	while (counter % 4) {
		os.put('=');
		++counter;
	}
}

void twee::io::base64Decode(std::istream& is, std::ostream& os)
{
	std::vector<int> T(256, -1);
	for (unsigned i = 0; i < 64; i++) {
		T[static_cast<std::size_t>(base64_encode_table[i])] =
			static_cast<int>(i);
	}

	int val = 0, valb = -8;
	for (auto c = is.get(); !is.eof(); c = is.get()) {
		int t = T[static_cast<unsigned>(c)];
		if (t == -1) break;
		val = (val << 6) + t;
		valb += 6;
		if (valb >= 0) {
			os.put(char((val >> valb) & 0xFF));
			valb -= 8;
		}
	}
}
