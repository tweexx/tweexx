/*
    Copyright © 2020 ezsh. All rights reserved.
    Use of this source code is governed by a Simplified BSD License which
    can be found in the LICENSE file.
*/

#ifndef TWEEXX_TYPES_HXX
#define TWEEXX_TYPES_HXX

#include <string>
#include <string_view>

namespace twee {
	using string = std::string;
	using string_view = std::string_view;
} // namespace twee

#endif // TWEEXX_TYPES_HXX
