/*
    Copyright © 2020 ezsh. All rights reserved.
    Use of this source code is governed by a Simplified BSD License which
    can be found in the LICENSE file.
*/

#include "./compiler.hxx"

#include "./constants.hxx"
#include "./escaping.hxx"
#include "./ifid.hxx"
#include "./impl/private_utils.hxx"
#include "./io.hxx"
#include "./logging.hxx"
#include "./modules.hxx"
#include "./story.hxx"

#include "twee-config.h"

#include "formats.hxx"

#include <boost/algorithm/string/join.hpp>
#include <boost/algorithm/string/replace.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <boost/lexical_cast.hpp>

#include <algorithm>
#include <sstream>
#include <string_view>
#include <vector>

namespace {
	namespace tags {
		constexpr const std::string_view twinePrivate{"Twine.private"};
	}
}

twee::Compiler::Compiler(const twee::StoryFormatsMap& formats, const twee::StoryCompilationOptions& options)
    : options_{options}
    , formats_{formats}
{
}

void twee::Compiler::toTwine2Archive(std::ostream& os, const twee::Story& story) const
{
	const auto& format = selectFormat(story);
	assert(format.isTwine2Style());
	getTwine2DataChunk(os, story, format, startPassageName(story));
	os << '\n';
}

void twee::Compiler::toTwine1Archive(std::ostream& os, const twee::Story& story) const
{
	// NOTE: In Twine 1.4, the passage data wrapper is part of the story formats
	// themselves, so we have to create/forge one here.  We use the Twine 1.4 vanilla
	// `storeArea` ID, rather than SugarCube's preferred `store-area` ID, for maximum
	// compatibility and interoperability.
	os << R"(<div id="storeArea" data-size=")" << twine1PublicPassagesCount(story) << R"(" hidden>)";
	getTwine1PassageChunk(os, story);
	os << "</div>\n";
}

void twee::Compiler::toHTML(std::ostream& os, const twee::Story& story) const
{
	const auto& format = selectFormat(story);
	if (format.isTwine2Style()) {
		toTwine2HTML(os, story);
	} else {
		toTwine1HTML(os, story);
	}
}

void twee::Compiler::toTwine2HTML(std::ostream& os, const twee::Story& story) const
{
	const auto& format = selectFormat(story);
	assert(format.isTwine2Style());
	std::string tmpl = format.source();

	if (story.name().empty() && !story.contains(specialPassageNames::storyTitle)) {
		spdlog::critical("Special passage \"StoryTitle\" not found.");
	}

	// Story instance replacements.
	// we are interested in three of them: {{STORY_NAME}}, </head>, and {{STORY_DATA}}
	// unfortunately, {{STORY_NAME}} can occur more than once, so we have to modify the template
	boost::algorithm::replace_all(tmpl, "{{STORY_NAME}}", htmlEscaped(story.name()));

	const string headClosingTag{"</head>"};
	const string storyDataPlaceholder{"{{STORY_DATA}}"};

	const auto headClosingPos = tmpl.find(headClosingTag);
	const auto storyDataPlaceholderPos = tmpl.find(storyDataPlaceholder);

	if (headClosingPos == tmpl.npos) { spdlog::critical("Could not find '</head>' in the file header"); }
	assert(storyDataPlaceholderPos > headClosingPos);

	// now we split the template into three parts
	std::string_view head{&tmpl[0], headClosingPos};
	std::string_view middle{&tmpl[headClosingPos], storyDataPlaceholderPos - headClosingPos};
	std::string_view tail{&tmpl[storyDataPlaceholderPos + storyDataPlaceholder.size()],
	    tmpl.size() - (storyDataPlaceholderPos + storyDataPlaceholder.size())};

	os << head;
	if (!story.modules().empty() || !options_.headFile.empty()) { injectHeadContent(os, story); }
	os << middle;
	getTwine2DataChunk(os, story, format, startPassageName(story));
	os << tail;
}

void twee::Compiler::toTwine1HTML(std::ostream& os, const twee::Story& story) const
{
	const auto& format = selectFormat(story);
	assert(format.isTwine1Style());
	const std::filesystem::path formatDir = format.fileName().parent_path();
	const std::filesystem::path parentDir = formatDir.parent_path();
	std::string tmpl = format.source();

	// Get the story data.
	std::ostringstream data;
	getTwine1PassageChunk(data, story);

	// Story format compiler byline replacement.
	const std::string twineryDotOrg = R"(<a href="http://twinery.org/")";
	auto pos = tmpl.find(twineryDotOrg);
	if (pos != std::string::npos) {
		tmpl.replace(pos, twineryDotOrg.size(), R"(<a href="https://gitlab.com/ezsh/tweexx/)");
		boost::algorithm::replace_first(tmpl, ">Twine</a>", ">Tweexx</a>");
	}

	const auto replaceWithFilecontents = [&](const std::string& searchStr, const std::filesystem::path& file) {
		const auto tmplPos = tmpl.find(searchStr);
		if (tmplPos != std::string::npos) {
			try {
				const auto component = io::readAll(file);
				tmpl.replace(tmplPos, searchStr.size(), component);
			} catch (std::runtime_error& ex) {
				spdlog::critical("error: {0}", ex.what());
			}
		}
	};

	// Story format component replacements (SugarCube).
	replaceWithFilecontents("\"USER_LIB\"", formatDir / "userlib.js");
	// Story format component replacements (Twine 1.4+ vanilla story formats).
	replaceWithFilecontents("\"ENGINE\"", parentDir / "engine.js");
	replaceWithFilecontents("\"SUGARCANE\"", formatDir / "code.js");
	replaceWithFilecontents("\"JONAH\"", formatDir / "code.js");

	const auto itJquery = story.metadata().twine1.settings.find("jquery");
	if (itJquery != story.metadata().twine1.settings.end() && itJquery->second == "on") {
		replaceWithFilecontents("\"JQUERY\"", parentDir / "jquery.js");
	}
	const auto itModernizr = story.metadata().twine1.settings.find("modernizr");
	if (itModernizr != story.metadata().twine1.settings.end() && itModernizr->second == "on") {
		replaceWithFilecontents("\"MODERNIZR\"", parentDir / "modernizr.js");
	}

	std::string startPassage = startPassageName(story);
	// Story instance replacements.
	if (startPassage == defaultStartName) { startPassage = ""; }
	boost::algorithm::replace_first(tmpl, "\"VERSION\"", std::string("Compiled with ") + APP_ID_STR);
	const std::string timePlaceholder = "\"TIME\"";
	const auto timePos = tmpl.find(timePlaceholder);
	if (timePos != std::string::npos) {
		std::ostringstream timeStr;
		auto t = std::time(nullptr);
		auto tm = *std::localtime(&t);
		timeStr << std::put_time(&tm, "%d-%m-%Y %H-%M-%S") << std::endl;
		tmpl.replace(timePos, timePlaceholder.size(), timeStr.str());
	}
	boost::algorithm::replace_first(tmpl, "\"START_AT\"", '"' + startPassage + '"');
	boost::algorithm::replace_first(
	    tmpl, "\"STORY_SIZE\"", '"' + boost::lexical_cast<std::string>(twine1PublicPassagesCount(story)) + '"');
	const std::string storyStr = "\"STORY\"";
	if (const auto stPos = tmpl.find(storyStr); stPos != std::string::npos) {
		// Twine/Twee ≥1.4 style story format.
		tmpl.replace(stPos, storyStr.size(), data.str());
	} else {
		// Twine/Twee <1.4 style story format.
		const auto footerFilename = formatDir / "footer.html";
		try {
			const auto footer =
				std::filesystem::exists(footerFilename) ? io::readAll(footerFilename) : "</div>\n</body>\n</html>\n";
			tmpl.append(data.str());
			tmpl.append(footer);
		} catch (std::runtime_error& ex) {
			spdlog::critical(ex.what());
		}
	}

	// IFID replacement.
	if (!story.metadata().ifid.empty()) {
		const std::string storeAreaSC = R"(<div id="store-area)";
		const auto saPos = tmpl.find(storeAreaSC);
		if (saPos != std::string::npos) {
			// SugarCube
			std::ostringstream newSA;
			newSA << "<!-- UUID://" << story.metadata().ifid << R"(// --><div id="store-area)";
			tmpl.replace(saPos, storeAreaSC.size(), newSA.str());
		} else {
			// Twine/Twee vanilla story formats.
			std::ostringstream newSA;
			newSA << "<!-- UUID://" << story.metadata().ifid << R"(// --><div id="storeArea)";
			boost::algorithm::replace_first(tmpl, R"(<div id="storeArea)", newSA.str());
		}
	}

	os << tmpl;
}

void twee::Compiler::getTwine2DataChunk(std::ostream& os, const twee::Story& story, const twee::StoryFormat& format,
    const twee::string& startingPassage) const
{
	// Check the IFID status.
	std::string ifid = story.metadata().ifid;
	if (ifid.empty()) {
		if (!story.metadata().twine1.legacyIFID.empty()) {
			/*
			    LEGACY
			*/
			spdlog::error("Story IFID not found; reusing \"ifid\" entry from the \"StorySettings\" special passage.");
			ifid = story.metadata().twine1.legacyIFID;
			/*
			    END LEGACY
			*/
		} else {
			spdlog::critical("Story IFID not found; generating one for your project.");
			try {
				ifid = newIFID();
			} catch (std::runtime_error& ex) {
				spdlog::critical("IFID generation failed; {}", ex.what());
			}
		}
		ifid = "\"ifid\": \"" + ifid + '"';
		const char* baseMsg = "Copy the following ";
		if (story.contains(specialPassageNames::storyData)) {
			ifid += ",";
			spdlog::critical("{0} line into the \"StoryData\" special passage's JSON block (at the top):\n\n\t"
			                 "{1}\n\n"
			                 "E.g., it should look something like the following:\n\n:: StoryData\n"
			                 "{{\n\t{1}\n}}\n\n",
			    baseMsg, ifid);
		} else {
			spdlog::critical("{0} \"StoryData\" special passage into one of your project's twee source files:\n\n:: "
			                 "StoryData\n{{\n\t{1}\n}}",
			    baseMsg, ifid);
		}
	}

	// Gather all script and stylesheet passages.
	std::vector<const Passage*> scripts;
	std::vector<const Passage*> stylesheets;
	std::vector<const Passage*> content;

	std::string startID;
	for (const auto& p: story.passages()) {
		if (p.tagsHas(tags::twinePrivate)) { continue; }
		if (p.tagsHas("script")) {
			scripts.push_back(&p);
		} else if (p.tagsHas("stylesheet")) {
			stylesheets.push_back(&p);
		} else {
			if (p.name() == specialPassageNames::storyTitle || p.name() == specialPassageNames::storyData) { continue; }

			// LEGACY
			// TODO: Should we actually drop an empty StorySettings passage?
			if (p.name() == specialPassageNames::storySettings && story.metadata().twine1.settings.empty()) {
				continue;
			}
			// END LEGACY

			content.push_back(&p);
			if (startingPassage == p.name()) { startID = boost::lexical_cast<std::string>(content.size()); }
		}
	}

	if (startID.empty()) { spdlog::critical("Could not find start passage '{}'", startingPassage); }

	std::string options;
	if (!story.metadata().twine2.options.empty()) {
		std::vector<std::string> opts;
		std::copy(
		    story.metadata().twine2.options.begin(), story.metadata().twine2.options.end(), std::back_inserter(opts));
		options = boost::algorithm::join(opts, " ");
	}

	// Add the <tw-storydata> wrapper.
	/*
	    <tw-storydata name="…" startnode="…" creator="…" creator-version="…" ifid="…"
	        zoom="…" format="…" format-version="…" options="…" hidden>…</tw-storydata>
	*/

	os << "<!-- UUID://" << ifid << "// -->"
	   << "<tw-storydata name=" << std::quoted(attrEscaped(story.name())) << " startnode=" << std::quoted(startID)
	   << " creator=" << std::quoted(attrEscaped(impl::toTitleCase(APP_NAME)))
	   << " creator-version=" << std::quoted(attrEscaped(APP_VER_STR)) << " ifid=" << std::quoted(attrEscaped(ifid))
	   << " zoom=" << std::quoted(boost::lexical_cast<std::string>(story.metadata().twine2.zoom))
	   << " format=" << std::quoted(format.name()) << " format-version=" << std::quoted(format.versionStr())
	   << " options=" << std::quoted(options) << " hidden>";


	// Prepare the style element.
	if (!stylesheets.empty()) {
		/*
		    <style role="stylesheet" id="twine-user-stylesheet" type="text/twine-css">…</style>
		*/
		os << R"(<style role="stylesheet" id="twine-user-stylesheet" type="text/twine-css">)";
		if (stylesheets.size() == 1) {
			os << stylesheets.front()->text();
		} else {
			unsigned pid = 1;
			for (const auto p: stylesheets) {
				os << "/* twine-user-stylesheet #" << pid << ": "
				   << p->sourceFilename().lexically_proximate(story.baseDir()) << " */\n"
				   << p->text();
				if (*p->text().rbegin() != '\n') { os << '\n'; }
				++pid;
			}
		}
		os << "</style>";
	}

	// Prepare the script element.
	if (!scripts.empty()) {
		/*
		    <script role="script" id="twine-user-script" type="text/twine-javascript">…</script>
		*/
		os << R"(<script role="script" id="twine-user-script" type="text/twine-javascript">)";
		if (scripts.size() == 1) {
			os << scripts[0]->text();
		} else if (scripts.size() > 1) {
			unsigned pid = 1;
			for (const auto p: scripts) {
				os << "/* twine-user-script #" << pid << ": "
				   << p->sourceFilename().lexically_proximate(story.baseDir()) << " */\n"
				   << p->text();
				if (*p->text().rbegin() != '\n') { os << '\n'; }
				++pid;
			}
		}
		os << "</script>";
	}

	// Prepare tw-tag elements.
	/*
	    <tw-tag name="…" color="…"></tw-tag>
	*/
	for (const auto& tc: story.metadata().twine2.tagColors) {
		os << "<tw-tag name=" << std::quoted(tc.first) << " color=" << std::quoted(tc.second) << "></tw-tag>";
	}

	// Prepare normal passage elements.
	unsigned pid = 1;

	for (const Passage* p: content) {
		p->toPassagedata(os, pid);
		++pid;
	}

	os << "</tw-storydata>";
}


void twee::Compiler::getTwine1PassageChunk(std::ostream& os, const twee::Story& story) const
{
	std::size_t count = 0;

	for (const auto& p: story.passages()) {
		if (p.tagsHas(tags::twinePrivate)) { continue; }

		++count;
		p.toTiddler(os, count);
	}
}

void twee::Compiler::injectHeadContent(std::ostream& os, const twee::Story& story) const
{
	if (options_.headFile.empty() && story.modules().empty()) { // nothing to modify
		return;
	}

	// 	bool anyTagsAdded = false;

	for (const auto& [filename, enconding]: story.modules()) {
		// trim module sources?
		if (loadModule(os, filename, story.baseDir(), enconding)) {
			// 			anyTagsAdded = true;
			os << '\n';
		}
	}

	if (!options_.headFile.empty()) {
		// encoding?
		const std::string source = boost::algorithm::trim_copy(io::readAll(options_.headFile));
		if (!source.empty()) {
			os << source << '\n';
			// 			anyTagsAdded = true;
			// td+ statsAddExternalFile(headFile)
		} else {
			spdlog::error("Can't load {}", fmt::streamed(options_.headFile));
		}
	}
}

const twee::StoryFormat& twee::Compiler::selectFormat(const twee::Story& story) const
{
	if (!options_.storyFormatId.empty()) { // provided via command line
		return formats_.getByID(options_.storyFormatId);

	} else if (story.metadata().twine2.format.empty()) {
		return formats_.getByID(defaultFormatID);
	} else {
		return formats_.getByTwine2NameAndVersion(story.metadata().twine2.format,
		    story.metadata().twine2.formatVersion.empty() ? defaultFormatVersion :
		                                                    story.metadata().twine2.formatVersion);
	}
}

const std::string& twee::Compiler::startPassageName(const twee::Story& story) const
{
	if (!options_.startingPassage.empty()) { return options_.startingPassage; }
	if (!story.metadata().twine2.start.empty()) { return story.metadata().twine2.start; }

	return defaultStartName;
}

std::size_t twee::Compiler::twine1PublicPassagesCount(const twee::Story& story) const
{
	return static_cast<std::size_t>(
		std::count_if(story.passages().begin(), story.passages().end(), [](const Passage& p) {
			return !p.tagsHas(tags::twinePrivate);
		}));
}
