/*
    Copyright © 2020 ezsh. All rights reserved.
    Use of this source code is governed by a Simplified BSD License which
    can be found in the LICENSE file.
*/

#ifndef TWEEXX_CONSTANTS_HXX
#define TWEEXX_CONSTANTS_HXX

#include <cstddef>
#include <cstdint>
#include <string>
#include <string_view>

namespace twee {
	const std::size_t InvalidIndex = static_cast<std::size_t>(-1);

	extern const std::string defaultFormatID;
	extern const std::string_view defaultFormatName;
	extern const std::string_view defaultFormatVersion;
	extern const std::string_view defaultOutFile;
	// 	defaultOutMode   = outModeHTML
	extern const std::string defaultStartName;

	namespace specialPassageNames {
		extern const std::string_view storyData;
		extern const std::string_view storyIncludes;
		extern const std::string_view storyTitle;
		extern const std::string_view storySettings;
	} // namespace specialPassageNames

	namespace passageMetadataKeys {
		constexpr inline std::string_view position{"position"};
		constexpr inline std::string_view size{"size"};
		constexpr inline std::string_view sourceFile{"source-file"};
	}

	namespace namedEncoding {
		extern const std::string_view utf8;
	}

	enum class TweeVersion: std::uint8_t { Twee1, Twee2, Twee3, Latest = Twee3 };
} // namespace twee

#endif
