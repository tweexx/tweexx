/*
    Copyright © 2020 ezsh. All rights reserved.
    Use of this source code is governed by a Simplified BSD License which
    can be found in the LICENSE file.
*/

#include "toolsupport.hxx"

#include "twee/logging.hxx"
#include "twee/story.hxx"

#include <dimcli/cli.h>
#include <dimcli/cli.cpp>

#include <cstring>
#include <system_error>

#ifndef WIN32
#include <unistd.h>
#define access_func ::access
#else
#include <io.h>
#define access_func ::_waccess
#define W_OK 2
#endif

namespace fs = std::filesystem;

void tweexx::utils::checkDirectoryExistsAndIsWritable(const std::filesystem::path& p, const std::string& dirId)
{
	if (!fs::exists(p)) {
			throw std::runtime_error(dirId + " directory '" + p.string() + "' does not exist");
		}
		if (!fs::is_directory(p)) {
			throw std::runtime_error(dirId + " path '" + p.string() + "' exists, but is not a directory");
		}
		if (access_func(p.c_str(), W_OK) != 0) {
			throw std::runtime_error(
			    dirId + " directory '" + p.string() + "' is not writable: " + std::system_category().default_error_condition(errno).message());
		}
}

tweexx::utils::LogOptions::LogOptions(Dim::Cli& cli)
	: severity_{spdlog::level::level_enum::info}
{
	using spdlog::level::level_enum;
	cli.opt(&severity_, "verbosity-level v", severity_)
	    .desc("Verbosity level")
	    .choice(level_enum::trace, "trace", "Maximal verbosity.")
	    .choice(level_enum::debug, "debug", "Debugging messages and more severe.")
	    .choice(level_enum::info, "info", "General info and errors.")
	    .choice(level_enum::warn, "warning", "Warnings and errors.")
	    .choice(level_enum::err, "error", "Errors only.")
	    .choice(level_enum::critical, "critical", "Only critical errors.")
	    .choice(level_enum::off, "off", "Turn off.");
	cli.opt(&logFile_, "log-file", logFile_).desc("Duplicate log messages to the specified file if non-empty.");
}

void tweexx::utils::LogOptions::apply()
{
	logger_ = twee::logging::setupLogger(logFile_);
	spdlog::set_default_logger(logger_);
	spdlog::set_level(severity_);
}

void tweexx::utils::printStoryStatistics(std::ostream& os,
    const twee::StoryStatistics& statistics, bool printFiles, bool printContentStats)
{
	const std::string indent = "  ";

	const auto printFileList = [&](const char* prefix, const std::vector<fs::path>& files) {
		os << indent << prefix << ": " << files.size() << std::endl;
		for (const auto& f: files) {
			os << indent << indent << f.string() << std::endl;
		}
	};
	if (printFiles) {
		os << "Processed files (in order)" << std::endl;
		printFileList("Project files", statistics.files.project);
		printFileList("External files", statistics.files.external);
		if (printContentStats) { os << std::endl; }
	}
	if (printContentStats) {
		os << "Statistics" << std::endl
			<< indent << "Total> Passages: " << statistics.counts.passages << std::endl
			<< indent << "Story> Passages: " << statistics.counts.storyPassages
			<< ", Words: " << statistics.counts.storyWords << std::endl;
	}
}
